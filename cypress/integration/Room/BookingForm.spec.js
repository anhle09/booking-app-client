/* eslint-disable no-undef */
/// <reference types="cypress" />

describe('Room/BookingForm - Not login', () => {
  beforeEach(() => {
    cy.intercept('GET', '*/rooms/*').as('getRoom')
    cy.visit('/room/61e27b723485392cd463fa78')
  })

  it('display price, fee, total', () => {
    cy.wait('@getRoom').then(({ response }) => {
      const room = response.body.data
      const price = room?.price.weekday
      const fee = room?.price.weekday * 0.1
      const total = price + fee
      cy.contains(price?.toLocaleString()).should('be.visible')
      cy.contains(fee?.toLocaleString()).should('be.visible')
      cy.contains(total?.toLocaleString()).should('be.visible')
    })
  })

  it('navigate to login when input clicked', () => {
    cy.get('input[name=anotherPhone]').click()
    cy.url().should('include', '/login')
  })
})

describe('Room/BookingForm - Successful booking', () => {
  beforeEach(() => {
    const { tokenKey, token } = Cypress.env()
    localStorage.setItem(tokenKey, token)
    cy.intercept('POST', '*/reservation').as('submitBooking')
    cy.visit('/room/61e27b723485392cd463fa78')
  })

  it('submit form', () => {
    cy.get('input[name=anotherPhone]').type('01234567')
    cy.get('button[type=submit]').click()
    cy.wait('@submitBooking').should(({ request, response }) => {
      const payload = request.body
      expect(Object.keys(payload).length).to.equal(8)
      expect(response.statusCode).to.equal(201)
    })
  })
})
