/// <reference types="cypress" />

describe('ClientProfile', () => {
  beforeEach(() => {
    const { tokenKey, token } = Cypress.env()
    localStorage.setItem(tokenKey, token)
    cy.visit('/')
    cy.get('header').within(() => {
      cy.get('nav button').click()
      cy.get('a[href="/booking-app/profile"]').click()
    })
    cy.url().should('include', '/profile')
  })

  it('display profile page', () => {
    cy.contains('Thông tin tài khoản').should('exist')
  })
})