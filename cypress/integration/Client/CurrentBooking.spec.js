/* eslint-disable no-undef */
/// <reference types="cypress" />

describe('Client/CurrentBooking', () => {
  beforeEach(() => {
    const { tokenKey, token } = Cypress.env()
    localStorage.setItem(tokenKey, token)
    cy.visit('/')
    cy.get('header').within(() => {
      cy.get('nav button').click()
      cy.get('a[href="/booking-app/booking"]').click()
    })
    cy.url().should('include', '/booking')
  })

  it('booking list is not empty', () => {
    cy.get('main').within(() => {
      // Booking item length > 0
      cy.get('li').should('have.length.gt', 0)

      // Display room image, name, address, dates, status
      cy.get('img').each(($img) => {
        expect($img[0].naturalWidth).to.be.greaterThan(0)
      })
      cy.get('data').each(($data) => {
        cy.wrap($data).should('not.be.empty')
      })
    })
  })

  it('booking list is empty', () => {
    cy.intercept('GET', '*/reservation', { data: [] })
    cy.reload()

    cy.get('main').within(() => {
      cy.get('li').should('have.length', 0)
    })
  })

})
