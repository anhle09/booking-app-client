/// <reference types="cypress" />

describe('AuthLogin', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('displays user menu', () => {
    cy.get('header').within(() => {
      cy.get('a[href*=login]').click()
    })
    cy.get('input[name=email]').type('admin@mail.com')
    cy.get('input[name=password]').type('admin')
    cy.get('button[type=submit]').click()
  })
})