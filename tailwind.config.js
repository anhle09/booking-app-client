module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {},
    maxWidth: {
      'sm': '640px',
      'md': '960px',
      'lg': '1100px',
      'xl': '1320px',
      '2xl': '1800px',
    },
    minWidth: {
      'sm': '640px',
      'md': '960px',
      'lg': '1100px',
      'xl': '1320px',
      '2xl': '1800px',
    }
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
  ],
}
