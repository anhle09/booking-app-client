import moment from 'moment'
import { useAppContext } from 'App/Context'
import LoadingPage from 'shared/components/LoadingPage'
import Navigation from 'shared/components/Navigation'
import { RESERVATION_URL } from 'shared/constants/ApiUrl'
import { SVG_REGISTER } from 'shared/constants/AssetUrl'
import { ReservationType } from 'shared/models/Reservation'
import { imageUrl } from 'shared/utils/luxstay'
import useApi from 'shared/hooks/api'
import { RoomType } from 'shared/models/Room'

export default function CurrentBooking() {
  const { userData } = useAppContext()
  const { response, isLoading } = useApi.get(`${RESERVATION_URL}?client=${userData?.id}`)
  if (isLoading) return <LoadingPage />
  const bookingList = response.data as ReservationType[]

  return (
    <>
      <Navigation />
      <main className='flex max-w-lg mx-auto p-5 sm:p-8'>
        <div className='flex-grow lg:mr-16'>
          <h2 className='mb-2'>Danh sách đặt phòng của bạn</h2>
          {bookingList?.length ? (
            <>
              <p>Hiện tại bạn đang có {response.total} đặt phòng</p>
              <ul className='mt-4 space-y-4'>
                {bookingList.map((item) => (
                  <BookingItem key={item.id} bookingData={item} />
                ))}
              </ul>
            </>
          ) : (
            <p>Bạn chưa có đặt phòng nào</p>
          )}
        </div>

        <div className='hidden lg:block lg:basis-[280px] xl:basis-[360px] shrink-0 mt-12'>
          <img src={SVG_REGISTER} alt='booking' />
        </div>
      </main>
    </>
  )
}

type BookingType = { bookingData: ReservationType }

function BookingItem({ bookingData }: BookingType) {
  const startDate = moment(bookingData.startDate).format('DD-MM-YYYY')
  const endDate = moment(bookingData.endDate).format('DD-MM-YYYY')
  const roomData = bookingData.room as RoomType

  return (
    <li className='flex max-w-[560px] p-4 border rounded-lg shadow-sm'>
      <img
        src={imageUrl(roomData.cover_image)}
        alt='room-cover'
        className='hidden sm:block h-36 aspect-square object-cover'
      />

      <section className='sm:ml-6'>
        <data className='font-medium'>{roomData.name}</data>
        <data className='mt-1 mb-2'>
          {roomData.address.ward}, {roomData.address.district}
        </data>

        <div className='flex space-x-1'>
          <h5 className='text-gray-700'>Ngày nhận phòng:</h5>
          <data>{startDate}</data>
        </div>

        <div className='flex space-x-1'>
          <h5 className='text-gray-700'>Ngày trả phòng:</h5>
          <data>{endDate}</data>
        </div>

        <div className='flex space-x-1'>
          <h5 className='text-gray-700'>Trạng thái:</h5>
          <data>{bookingData.status}</data>
        </div>
      </section>
    </li>
  )
}
