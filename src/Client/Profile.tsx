import { useState } from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { UserCircleIcon } from '@heroicons/react/solid'
import { useAppContext } from 'App/Context'
import { FieldText } from 'shared/components/Form'
import Navigation from 'shared/components/Navigation'
import useApi from 'shared/hooks/api'
import Spinner from 'shared/components/Spinner'
import ApiStatus from 'shared/components/ApiStatus'
import { SVG_REGISTER } from 'shared/constants/AssetUrl'

const formValidate = Yup.object({
  firstName: Yup.string().required('Required').min(2, 'Too short'),
  phone: Yup.string().required('Required').min(8, 'Too short'),
  email: Yup.string().email('Email must be a valid email').required('Required'),
  password: Yup.string().min(6, 'Password must be at least 6 characters'),
})

export default function Profile() {
  const { userData, handleUserData } = useAppContext()
  const [apiRes, patchRequest] = useApi.multipartPatch(`/users/${userData?.id}`)
  const [avatar, setAvatar] = useState('')
  const userAvatar = avatar || userData?.avatar

  const formOnSubmit = async (values: any) => {
    const res = await patchRequest(values)
    if (res.data) {
      handleUserData(res.data)
    }
  }

  const handleReadFile = (event: any) => {
    const files = event.target.files
    const fileReader = new FileReader()
    fileReader.onload = (e: any) => {
      const photoDataUrl = e.target.result
      setAvatar(photoDataUrl)
    }
    fileReader.readAsDataURL(files[0])
  }

  const formInit: any = {
    firstName: userData?.firstName || '',
    lastName: userData?.lastName || '',
    phone: userData?.phone || '',
    email: userData?.email || '',
  }

  return (
    <>
      <Navigation />
      <main className='flex items-center max-w-lg mx-auto p-5 sm:p-8'>
        <section className='flex-grow md:mr-16'>
          <h1>Thông tin tài khoản</h1>
          <h4 className='mt-1 text-gray-500'>
            Cập nhật thông tin tài khoản của bạn
          </h4>

          <Formik
            enableReinitialize
            initialValues={formInit}
            validationSchema={formValidate}
            onSubmit={formOnSubmit}
          >
            {({ touched, errors, setFieldValue }) => (
              <Form>
                <div className='mt-4 flex items-center'>
                  {userAvatar ? (
                    <img
                      src={userAvatar}
                      className='h-24 w-24 rounded-full object-cover mr-4'
                      alt='avatar'
                    />
                  ) : (
                    <UserCircleIcon className='text-gray-400 h-24 mr-2' />
                  )}

                  <label>
                    <span className='underline font-medium text-sm cursor-pointer'>
                      Đổi ảnh đại diện
                    </span>
                    <input
                      type='file'
                      accept='.jpg, .png'
                      className='sr-only'
                      onChange={(event) => {
                        setFieldValue('files', event.target.files)
                        handleReadFile(event)
                      }}
                    />
                  </label>
                </div>

                <div className='grid sm:grid-cols-2 sm:gap-4'>
                  <div className='mt-4'>
                    <FieldText
                      label='Tên của bạn'
                      name='firstName'
                      error={touched.firstName && errors.firstName}
                    />
                  </div>
                  <div className='mt-4'>
                    <FieldText
                      label='Họ và tên đệm'
                      name='lastName'
                      placeholder='option'
                    />
                  </div>
                </div>

                <div className='mt-4'>
                  <FieldText
                    label='Số điện thoại'
                    name='phone'
                    error={touched.phone && errors.phone}
                  />
                </div>

                <div className='mt-4'>
                  <FieldText
                    label='Địa chỉ email'
                    type='email'
                    name='email'
                    error={touched.email && errors.email}
                  />
                </div>

                <div className='my-6'>
                  <button
                    type='submit'
                    className='button px-8 float-right space-x-2'
                  >
                    <span>Cập nhật</span>
                    {apiRes.status.loading && <Spinner size='sm' />}
                  </button>
                  <ApiStatus apiStatus={apiRes.status} />
                </div>
              </Form>
            )}
          </Formik>
        </section>

        <section className='hidden md:block md:basis-[280px] lg:basis-[400px] shrink-0'>
          <img src={SVG_REGISTER} alt='profile' />
        </section>
      </main>
    </>
  )
}
