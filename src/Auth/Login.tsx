import { Link, useNavigate, useLocation } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAppContext } from 'App/Context'
import { storeAuthToken } from 'shared/utils/authToken'
import { FieldText } from 'shared/components/Form'
import Navigation from 'shared/components/Navigation'
import useApi from 'shared/hooks/api'
import ApiStatus from 'shared/components/ApiStatus'
import Spinner from 'shared/components/Spinner'
import { LOGIN_URL } from 'shared/constants/ApiUrl'
import { SVG_LOGIN } from 'shared/constants/AssetUrl'
import { PasswordField } from './ChangePassword'

const formValidate = Yup.object({
  email: Yup.string().email('Email must be a valid email').required('Required'),
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

export default function Login() {
  const navigate = useNavigate()
  const { state } = useLocation() // nullable
  const { handleUserData } = useAppContext()
  const [apiRes, postRequest] = useApi.post(LOGIN_URL)

  const formOnSubmit = async (values: any) => {
    const { data } = await postRequest(values)
    if (data) {
      handleUserData(data.user)
      storeAuthToken(data.token)
      navigate((state as string) || '/', { replace: true })
    }
  }

  return (
    <>
      <Navigation />
      <main className='flex items-center max-w-lg mx-auto p-5 sm:p-8'>
        <section className='hidden md:block md:basis-[280px] lg:basis-[400px] shrink-0'>
          <img src={SVG_LOGIN} alt='login' />
        </section>

        <section className='flex-grow md:ml-16'>
          <div className='sm:p-12 sm:border sm:rounded-lg sm:shadow'>
            <h1>Đăng nhập</h1>
            <h4 className='mt-1 text-gray-500'>
              Đăng nhập để quản lí tài khoản của bạn
            </h4>

            <Formik
              initialValues={{ email: 'admin@mail.com', password: 'admin' }}
              validationSchema={formValidate}
              onSubmit={formOnSubmit}
            >
              {({ touched, errors }) => (
                <Form>
                  <div className='mt-6'>
                    <FieldText
                      label='Địa chỉ email'
                      type='email'
                      name='email'
                      error={touched.email && errors.email}
                    />
                  </div>

                  <div className='mt-6'>
                    <PasswordField
                      label='Mật khẩu'
                      name='password'
                      error={touched.password && errors.password}
                    />
                  </div>

                  <div className='mt-8 flex justify-between'>
                    {apiRes.status.errorMessage ? (
                      <ApiStatus apiStatus={apiRes.status} />
                    ) : (
                      <h6 className='mt-1 mr-4'>
                        Bạn chưa có tài khoản?{' '}
                        <Link to='/register' className='text-indigo-600'>
                          Đăng ký
                        </Link>
                      </h6>
                    )}
                    <button
                      type='submit'
                      className='shrink-0 button px-4 md:px-8 space-x-2'
                    >
                      <span>Đăng nhập</span>
                      {apiRes.status.loading && <Spinner size='sm' />}
                    </button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </section>
      </main>
    </>
  )
}
