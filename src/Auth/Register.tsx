import { Link, useNavigate } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAppContext } from 'App/Context'
import { storeAuthToken } from 'shared/utils/authToken'
import { FieldText } from 'shared/components/Form'
import Navigation from 'shared/components/Navigation'
import useApi from 'shared/hooks/api'
import Spinner from 'shared/components/Spinner'
import ApiStatus from 'shared/components/ApiStatus'
import { REGISTER_URL } from 'shared/constants/ApiUrl'
import { SVG_REGISTER } from 'shared/constants/AssetUrl'
import { PasswordField } from './ChangePassword'

const formValidate = Yup.object({
  firstName: Yup.string().required('Required').min(2, 'Too short'),
  email: Yup.string().email('Invalid email').required('Required'),
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

export default function Register() {
  const navigate = useNavigate()
  const { handleUserData } = useAppContext()
  const [apiRes, postRequest] = useApi.post(REGISTER_URL)

  const formOnSubmit = async (values: any) => {
    const { data } = await postRequest(values)
    if (data) {
      handleUserData(data.user)
      storeAuthToken(data.token)
      navigate('/', { replace: true })
    }
  }

  return (
    <>
      <Navigation />
      <main className='flex items-center max-w-lg mx-auto p-5 sm:p-8 sm:mt-6'>
        <section className='flex-grow md:mr-16'>
          <div className='sm:px-12 sm:py-10 sm:border sm:rounded-lg sm:shadow'>
            <h1>Đăng ký thành viên</h1>
            <h4 className='mt-1 text-gray-500'>
              Đăng ký ngay để tích điểm thưởng và nhận ưu đải
            </h4>

            <Formik
              initialValues={{ firstName: '', email: '', password: '' }}
              validationSchema={formValidate}
              onSubmit={formOnSubmit}
            >
              {({ touched, errors }) => (
                <Form>
                  <div className='mt-4'>
                    <FieldText
                      sm
                      label='Tên của bạn'
                      name='firstName'
                      error={touched.firstName && errors.firstName}
                    />
                  </div>

                  <div className='mt-4'>
                    <FieldText
                      sm
                      label='Địa chỉ email'
                      type='email'
                      name='email'
                      error={touched.email && errors.email}
                    />
                  </div>

                  <div className='mt-4'>
                    <PasswordField
                      sm
                      label='Mật khẩu'
                      name='password'
                      error={touched.password && errors.password}
                    />
                  </div>

                  <div className='mt-6 flex justify-between'>
                    {apiRes.status.errorMessage ? (
                      <ApiStatus apiStatus={apiRes.status} />
                    ) : (
                      <h6 className='mt-1 mr-4'>
                        Bạn đã có tài khoản?{' '}
                        <Link to='/login' className='text-indigo-600'>
                          Đăng nhập
                        </Link>
                      </h6>
                    )}
                    <button
                      type='submit'
                      className='shrink-0 button px-8 space-x-2'
                    >
                      <span>Đăng ký</span>
                      {apiRes.status.loading && <Spinner size='sm' />}
                    </button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </section>

        <section className='hidden md:block md:basis-[280px] lg:basis-[420px] shrink-0'>
          <img src={SVG_REGISTER} alt='register' />
        </section>
      </main>
    </>
  )
}
