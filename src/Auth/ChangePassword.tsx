import { EyeIcon, EyeOffIcon } from '@heroicons/react/outline'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAppContext } from 'App/Context'
import { FieldText } from 'shared/components/Form'
import { CHANGE_PASSWORD_URL } from 'shared/constants/ApiUrl'
import { SVG_LOGIN } from 'shared/constants/AssetUrl'
import useApi from 'shared/hooks/api'
import Navigation from 'shared/components/Navigation'
import ApiStatus from 'shared/components/ApiStatus'
import Spinner from 'shared/components/Spinner'
import { useState } from 'react'

const formValidate = Yup.object({
  password: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
  newPassword: Yup.string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters'),
})

export default function ChangePassword() {
  const { userData } = useAppContext()
  const [apiRes, postRequest] = useApi.post(CHANGE_PASSWORD_URL)

  const formInit = {
    email: userData?.email,
    password: '',
    newPassword: '',
  }

  return (
    <>
      <Navigation />
      <main className='flex items-center max-w-lg mx-auto p-5 sm:p-8'>
        <section className='hidden md:block md:basis-[280px] lg:basis-[400px] shrink-0'>
          <img src={SVG_LOGIN} alt='change-password' />
        </section>

        <section className='flex-grow md:ml-16'>
          <div className='sm:p-12 sm:border sm:rounded-lg sm:shadow'>
            <h1>Đổi mật khẩu</h1>
            <h4 className='mt-1 text-gray-500'>Nhập mật khẩu để xác nhận</h4>

            <Formik
              initialValues={formInit}
              validationSchema={formValidate}
              onSubmit={postRequest}
            >
              {({ touched, errors }) => (
                <Form>
                  <div className='mt-6'>
                    <PasswordField
                      label='Mật khẩu'
                      name='password'
                      error={touched.password && errors.password}
                    />
                  </div>

                  <div className='mt-6'>
                    <PasswordField
                      label='Mật khẩu mới'
                      name='newPassword'
                      error={touched.newPassword && errors.newPassword}
                    />
                  </div>

                  <div className='mt-8 h-8'>
                    <button
                      type='submit'
                      className='button px-4 md:px-8 space-x-2 float-right'
                    >
                      <span>Lưu</span>{' '}
                      {apiRes.status.loading && <Spinner size='sm' />}
                    </button>
                    <ApiStatus apiStatus={apiRes.status} />
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </section>
      </main>
    </>
  )
}

type PwFieldType = { label: string; name: string; error: any; sm?: boolean }

export function PasswordField(props: PwFieldType) {
  const [hide, setHide] = useState(true)
  const toggleHide = () => setHide((prev) => !prev)

  return (
    <div className='relative'>
      <FieldText {...props} type={hide ? 'password' : 'text'} />
      <span
        onClick={toggleHide}
        className='absolute right-4 cursor-pointer'
        style={{ top: props.sm ? 43 : 48 }}
      >
        {hide ? (
          <EyeIcon width={20} color='gray' />
        ) : (
          <EyeOffIcon width={20} color='gray' />
        )}
      </span>
    </div>
  )
}
