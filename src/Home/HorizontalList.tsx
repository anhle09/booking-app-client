import { useState, useEffect } from 'react'
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline'
import useScrollHorizontal from '../shared/hooks/scrollHorizontal'

export default function HorizontalList({ children }: any) {
  const { divRef, handleScrollLeft, handleScrollRight } = useScrollHorizontal()
  const [scrollButtonVisible, setScrollButtonVisible] = useState(false)

  useEffect(() => {
    const handleResize = () => {
      const element: any = divRef.current
      const { scrollWidth, offsetWidth } = element
      if (offsetWidth < scrollWidth) {
        if (!scrollButtonVisible) {
          setScrollButtonVisible(true)
        }
      } else {
        if (scrollButtonVisible) {
          setScrollButtonVisible(false)
        }
      }
    }
    handleResize()
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [divRef, scrollButtonVisible])

  return (
    <div className='relative'>
      {scrollButtonVisible && (
        <div className='hidden sm:block absolute top-[-3rem] right-0'>
          <div className='flex'>
            <button onClick={() => handleScrollLeft(236)} className='btn-icon'>
              <ChevronLeftIcon className='h-5' />
            </button>
            <button onClick={() => handleScrollRight(236)} className='btn-icon'>
              <ChevronRightIcon className='h-5' />
            </button>
          </div>
        </div>
      )}
      <div ref={divRef} className='flex overflow-auto space-x-4 scrollbar-none'>
        {children}
      </div>
    </div>
  )
}
