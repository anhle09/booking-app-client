import { Link } from 'react-router-dom'
import { TopLocation } from 'shared/constants/TopLocation'
import HorizontalList from './HorizontalList'

export default function SuggestionList() {
  return (
    <HorizontalList>
      {TopLocation.map((item) => (
        <LocationCard key={item.id} item={item} />
      ))}
    </HorizontalList>
  )
}

const LocationCard = ({ item }: any) => (
  <Link
    to={`/room?${item.type}=${item.id}`}
    className='w-full min-w-[220px] h-[240px] sm:h-[300px]'
  >
    <div
      className='h-full pt-[190px] sm:pt-[240px] rounded-md'
      style={{ backgroundImage: `url(${item.image})` }}
    >
      <h2 className='mx-3 text-white'>{item.name}</h2>
    </div>
  </Link>
)
