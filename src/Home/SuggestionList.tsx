import { Link } from 'react-router-dom'
import { Suggestion } from 'shared/constants/TopLocation'
import HorizontalList from './HorizontalList'

export default function SuggestionList() {
  return (
    <HorizontalList>
      {Suggestion.map((item) => (
        <RoomCard key={item.id} item={item} />
      ))}
    </HorizontalList>
  )
}

const RoomCard = ({ item }: any) => (
  <Link to={`/room/${item.id}`} className='w-full min-w-[220px]'>
    <img src={item.image} alt='room-cover' className='rounded' />
    <h4 className='mt-2'>{item.name}</h4>
  </Link>
)
