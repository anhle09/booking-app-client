import Navigation from 'shared/components/Navigation'
import Footer from 'shared/components/Footer'
import LocationList from './LocationList'
import SuggestionList from './SuggestionList'
import { IMG_BANNER, IMG_BANNER_CROP } from 'shared/constants/AssetUrl'

export default function Home() {
  return (
    <>
      <Navigation searchBox />
      <main className='max-w-xl mx-auto p-5 space-y-6 sm:space-y-8'>
        <section className='sm:mt-4'>
          <img
            src={IMG_BANNER_CROP}
            className='sm:hidden rounded-lg'
            alt='banner'
          />
          <img
            src={IMG_BANNER}
            className='hidden sm:block rounded-lg'
            alt='banner'
          />
        </section>

        <section className='space-y-2'>
          <h1>Chào mừng đến với Luxstay</h1>
          <h6>
            Đặt chỗ ở, homestay, cho thuê xe, trải nghiệm và nhiều hơn nữa trên
            Luxstay
          </h6>
          <h6>Đăng nhập hoặc Đăng ký để trải nghiệm nhiều hơn</h6>
        </section>

        <section className='space-y-2'>
          <h2>Địa điểm nổi bật</h2>
          <h6 className='hidden sm:block'>
            Cùng Luxstay bắt đầu chuyến hành trình chinh phục thế giới của bạn
          </h6>
          <div className='pt-4'>
            <LocationList />
          </div>
        </section>

        <section className='space-y-2'>
          <h2>Gợi ý từ Luxstay</h2>
          <h6>Những địa điểm thường đến mà Luxstay gợi ý dành cho bạn</h6>
          <div className='pt-4'>
            <SuggestionList />
          </div>
        </section>
      </main>
      <Footer />
    </>
  )
}
