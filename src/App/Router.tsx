import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import ContextProvider from './Context'
import ProtectedRoute from 'App/ProtectedRoute'
import NotFoundPage from 'shared/components/NotFoundPage'

import Home from 'Home'
import RoomPage from 'Room'
import RoomList from 'RoomList'

import Login from 'Auth/Login'
import Register from 'Auth/Register'
import ChangePassword from 'Auth/ChangePassword'
import Profile from 'Client/Profile'

import HostLayout from 'Host'
import RoomCreateLayout from 'Host/RoomCreate'
import RoomInfo from 'Host/RoomCreate/RoomInfo'
import RoomAddress from 'Host/RoomCreate/RoomAddress'
import RoomPrice from 'Host/RoomCreate/RoomPrice'
import Listing from 'Host/Listing'
import Reservation from 'Host/Reservation'
import CurrentBooking from 'Client/CurrentBooking'

export default function Router() {
  return (
    <BrowserRouter basename='booking-app'>
      <ContextProvider>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='room' element={<RoomList />} />
          <Route path='room/:id' element={<RoomPage />} />
          <Route path='login' element={<Login />} />
          <Route path='register' element={<Register />} />

          <Route element={<ProtectedRoute />}>
            <Route path='change-password' element={<ChangePassword />} />
            <Route path='profile' element={<Profile />} />
            <Route path='booking' element={<CurrentBooking />} />

            <Route path='host' element={<HostLayout />}>
              <Route index element={<Navigate replace to='listing' />} />
              <Route path='listing' element={<Listing />} />
              <Route path='reservation' element={<Reservation />} />
              <Route path='create' element={<RoomCreateLayout />}>
                <Route index element={<RoomInfo />} />
                <Route path='room-info/:roomId' element={<RoomInfo />} />
                <Route path='room-address/:roomId' element={<RoomAddress />} />
                <Route path='room-price/:roomId' element={<RoomPrice />} />
              </Route>
            </Route>
          </Route>

          <Route path='*' element={<NotFoundPage />} />
        </Routes>
      </ContextProvider>
    </BrowserRouter>
  )
}
