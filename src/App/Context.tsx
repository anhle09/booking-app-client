import React, { useState, useEffect, ReactNode } from 'react'
import { useNavigate } from 'react-router-dom'
import { UserType } from 'shared/models/User'
import { api } from 'shared/utils/api'
import { HOST_URL, CURRENT_USER_URL } from 'shared/constants/ApiUrl'
import { getStoredAuthToken, removeStoredAuthToken } from 'shared/utils/authToken'
import LoadingPage from 'shared/components/LoadingPage'

type ContextType = {
  userData?: UserType
  handleUserData: (data: UserType) => void
  handleLogOut: () => void
}

const AppContext = React.createContext<ContextType>(null!)

export function useAppContext() {
  return React.useContext(AppContext)
}

type PropsType = { children: ReactNode }

export default function ContextProvider({ children }: PropsType) {
  const navigate = useNavigate()
  const [userData, setUserData] = useState<UserType>()
  const [isLoading, setIsLoading] = useState(true)

  const handleUserData = (data: UserType) => {
    if (data.avatar) {
      data.avatar = HOST_URL + data.avatar
    }
    setUserData(data)
  }

  useEffect(() => {
    const getCurrentUser = async () => {
      if (getStoredAuthToken()) {
        const res = await api.get(CURRENT_USER_URL)
        if (res.data) handleUserData(res.data)
      }
      setIsLoading(false)
    }
    getCurrentUser()
  }, [])

  if (isLoading) return <LoadingPage />

  const handleLogOut = () => {
    removeStoredAuthToken()
    setUserData(undefined)
    navigate('/', { replace: true })
  }

  const contextValue = { userData, handleUserData, handleLogOut }

  return <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
}
