import { Outlet, Navigate, useLocation } from 'react-router-dom'
import { useAppContext } from 'App/Context'

export default function ProtectedRoute() {
  let { userData } = useAppContext()
  let { pathname } = useLocation()

  if (!userData) {
    return <Navigate to='/login' replace state={pathname} />
  }
  return <Outlet />
}
