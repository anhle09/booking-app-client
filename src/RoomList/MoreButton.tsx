import { useRef } from 'react'
import Spinner from 'shared/components/Spinner'
import useQueryParam from 'shared/hooks/queryParam'

export default function MoreButton({ isLoading = false }) {
  const { queryParam, setQueryParam } = useQueryParam()
  const page = useRef(1)

  const handleClick = () => {
    page.current++
    setQueryParam({ ...queryParam, limit: page.current * 8 })
  }

  return (
    <div className='text-center'>
      <button className='btn-outlined px-8' onClick={handleClick}>
        Xem thêm {isLoading && <Spinner size='sm' />}
      </button>
    </div>
  )
}
