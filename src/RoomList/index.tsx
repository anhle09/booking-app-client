import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import useApi from 'shared/hooks/api'
import Navigation from 'shared/components/Navigation'
import Footer from 'shared/components/Footer'
import LoadingPage from 'shared/components/LoadingPage'
import RoomItem from './RoomItem'
import Filter from './Filter'
import MoreButton from './MoreButton'
import { RoomType } from 'shared/models/Room'

export default function RoomList() {
  const { search } = useLocation() // return '?abc'
  const [roomList, setRoomList] = useState<RoomType[]>([])
  const { response, isLoading } = useApi.get(`/rooms${search}`, false)

  useEffect(() => {
    const roomData = response.data
    if (roomData?.length) {
      setRoomList(roomData)
    }
  }, [response.data])

  if (!roomList.length) return <LoadingPage />
  return (
    <>
      <Navigation searchBox />
      <main className='max-w-xl mx-auto mt-6 sm:mt-8 px-4 space-y-6'>
        <section className='space-y-2'>
          <h2>Danh sách homestay</h2>
          <h6>
            Trước khi đặt phòng, hãy kiểm tra những địa điểm bị hạn chế du lịch trong thời
            gian này
          </h6>
          <div className='hidden sm:flex justify-end w-full'>
            <Filter />
          </div>
        </section>

        <section className='grid grid-cols-2 md:grid-cols-4 gap-2 sm:gap-4'>
          {roomList?.map((item: any) => (
            <RoomItem key={item.id} roomData={item} />
          ))}
        </section>
        <MoreButton isLoading={isLoading} />
      </main>
      <Footer />
    </>
  )
}
