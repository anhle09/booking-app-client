import { Link } from "react-router-dom"
import { RoomType } from "shared/models/Room"
import { imageUrl } from "shared/utils/luxstay"

type Props = { roomData: RoomType }

export default function RoomItem({ roomData }: Props) {
  return (
    <Link key={roomData.id}
      to={`/room/${roomData.id}`}
      className="mb-2 sm:mb-4 rounded shadow cursor-pointer"
    >
      <img alt=""
        src={imageUrl(roomData.cover_image)}
        className="rounded object-cover aspect-[3/2]"
      />
      <div className="m-3 text-gray-900">
        <h4 className="h-6 mb-1 overflow-hidden">{roomData.name}</h4>
        <h5>{roomData.price.weekday.toLocaleString()} đ/đêm</h5>
      </div>
    </Link>
  )
}
