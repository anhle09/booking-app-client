import { useRef, useState } from 'react'
import { Menu } from '@headlessui/react'
import useQueryParam from 'shared/hooks/queryParam'

export default function PriceFilter() {
  const range = useRef({ min: 0, max: 10000000 })
  const { queryParam, setQueryParam } = useQueryParam()

  const handleSliderChange = (value: { min: number; max: number }) => {
    range.current = value
  }

  const handleFilter = () => {
    setQueryParam({
      ...queryParam,
      min: range.current.min,
      max: range.current.max,
    })
  }

  const handleResetFilter = () => {
    delete queryParam.min
    delete queryParam.max
    setQueryParam({ ...queryParam })
  }

  return (
    <Menu as='div' className='relative inline-block mx-1'>
      <Menu.Button className='btn-base'>Price</Menu.Button>
      <Menu.Items
        as='div'
        className='absolute z-30 left-0 w-72 mt-2 px-4 py-6 bg-white rounded-md shadow-lg border'
      >
        <h5>Room price</h5>
        <PriceSlider max={10000000} handleChange={handleSliderChange} />

        <div className='flex items-center justify-between mt-5'>
          <span className='cursor-pointer' onClick={handleResetFilter}>
            Reset
          </span>
          <button className='button' onClick={handleFilter}>
            Apply
          </button>
        </div>
      </Menu.Items>
    </Menu>
  )
}

type SliderProps = {
  min?: number
  max: number
  handleChange: (value: { min: number; max: number }) => void
}

function PriceSlider({ min = 0, max, handleChange }: SliderProps) {
  let [rangeInput, setRangeInput] = useState({ first: 0, second: 100 })
  let [range, setRange] = useState({ left: 0, right: 100, min: min, max: max })

  const handleInputChange = (value: { first?: number; second?: number }) => {
    rangeInput = { ...rangeInput, ...value }
    if (rangeInput.first < rangeInput.second) {
      range.left = rangeInput.first
      range.right = rangeInput.second
    } else {
      range.left = rangeInput.second
      range.right = rangeInput.first
    }
    range.min = Math.round((range.left / 100) * (max - min))
    range.max = Math.round((range.right / 100) * (max - min))

    setRange({ ...range })
    setRangeInput({ ...rangeInput })
    handleChange({ min: range.min, max: range.max })
  }

  return (
    <div className='mt-4 space-y-4'>
      <div className='relative h-2'>
        <input
          type='range'
          min='0'
          max='100'
          className='range-double'
          value={rangeInput.first}
          onChange={(e) => handleInputChange({ first: parseInt(e.target.value) })}
        />
        <input
          type='range'
          min='0'
          max='100'
          className='range-double'
          value={rangeInput.second}
          onChange={(e) => handleInputChange({ second: parseInt(e.target.value) })}
        />
        <div className='absolute inset-x-0 h-[6px] rounded-sm bg-gray-200' />
        <div
          className='absolute h-[6px] rounded-sm bg-indigo-500'
          style={{ left: `${range.left}%`, right: `${100 - range.right}%` }}
        />
      </div>

      <div className='flex space-x-4'>
        <label>
          <h5 className='pb-1'>Min</h5>
          <input
            type='text'
            className='textbox'
            value={range.min.toLocaleString() + ' đ'}
          />
        </label>
        <label>
          <h5 className='pb-1'>Max</h5>
          <input
            type='text'
            className='textbox'
            value={range.max.toLocaleString() + ' đ'}
          />
        </label>
      </div>
    </div>
  )
}

/* const [thumbOne, setThumbOne] = React.useState(0)
  const [thumbTwo, setThumbTwo] = React.useState(100)

  React.useEffect(() => {
    if (thumbOne < thumbTwo) {
      range.left = thumbOne
      range.right = thumbTwo
    }
    else {
      range.left = thumbTwo
      range.right = thumbOne
    }
    range.min = Math.round(range.left / 100 * (max - min))
    range.max = Math.round(range.right / 100 * (max - min))
    setRange({ ...range })
  }, [thumbOne, thumbTwo])

  const handleChangeThumbOne = (value: number) => {
    setThumbOne(value)
    handleChange(value, thumbTwo)
  }

  const handleChangeThumbTwo = (value: number) => {
    setThumbTwo(value)
    handleChange(thumbOne, value)
  }
*/
