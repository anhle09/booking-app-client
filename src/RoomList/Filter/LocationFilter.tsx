import { useState } from 'react'
import { Menu } from '@headlessui/react'
import { LocationMarkerIcon } from '@heroicons/react/outline'
import useQueryParam from 'shared/hooks/queryParam'
import { DistrictList, WardList } from 'shared/constants/AddressBook'

export default function LocationFilter() {
  const [selectedLocation, setSelectedLocation] = useState('Location')
  const { queryParam, setQueryParam } = useQueryParam()
  const { province, district } = queryParam
  const active = selectedLocation !== 'Location'
  let locationList: any[] = []

  if (province) {
    locationList = DistrictList.filter((item) => item.provinceId == province)
  } else if (district) {
    locationList = WardList.filter((item) => item.districtId == district)
  }

  const handleFilter = (location: any) => {
    setSelectedLocation(location.name)
    if (province) setQueryParam({ ...queryParam, district: location.id })
    else setQueryParam({ ...queryParam, ward: location.id })
  }

  const handleResetFilter = () => {
    setSelectedLocation('Location')
    delete queryParam.district
    delete queryParam.ward
    setQueryParam({ ...queryParam })
  }

  return (
    <Menu as='div' className='relative inline-block mx-1'>
      <Menu.Button
        className={`btn-base ${
          active ? 'bg-indigo-600 text-white' : 'bg-white text-gray-600'
        }`}
      >
        {selectedLocation}
      </Menu.Button>

      <Menu.Items
        as='ul'
        className='absolute z-30 left-0 w-56 mt-2 py-2 bg-white rounded-md shadow-lg border'
      >
        {locationList.map((item) => (
          <Menu.Item
            key={item.id}
            as='li'
            className='flex items-center py-2 px-4 text-sm text-gray-700 hover:bg-gray-100'
            onClick={() => handleFilter(item)}
          >
            <LocationMarkerIcon className='h-4 mr-2 text-gray-500' />
            <span>{item.name}</span>
          </Menu.Item>
        ))}

        <Menu.Item as='div' className='flex items-center justify-between p-4'>
          <span className='cursor-pointer' onClick={handleResetFilter}>
            Reset
          </span>
          <button className='button'>Apply</button>
        </Menu.Item>
      </Menu.Items>
    </Menu>
  )
}
