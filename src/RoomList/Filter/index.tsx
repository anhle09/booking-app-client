import { useState } from 'react'
import useQueryParam from 'shared/hooks/queryParam'
import LocationFilter from './LocationFilter'
import PriceFilter from './PriceFilter'

export default function Filter() {
  return (
    <>
      <CancellationFilter />
      <LocationFilter />
      <PriceFilter />
      <SortBySelect />
    </>
  )
}

function CancellationFilter() {
  const [active, setActive] = useState(false)
  const { queryParam, setQueryParam } = useQueryParam()

  const handleFilter = () => {
    if (queryParam.cancel) {
      delete queryParam.cancel
      setQueryParam({ ...queryParam })
    } else {
      setQueryParam({ ...queryParam, cancel: 1 })
    }
    setActive((prev) => !prev)
  }

  return (
    <button
      className={`btn-base ${
        active ? 'bg-indigo-600 text-white' : 'bg-white text-gray-600'
      }`}
      onClick={handleFilter}
    >
      Free cancellation
    </button>
  )
}

function SortBySelect() {
  const { queryParam, setQueryParam } = useQueryParam()
  const handleSortBy = (e: any) => {
    setQueryParam({ ...queryParam, sortBy: e.target.value })
  }

  return (
    <select onChange={handleSortBy} className='mx-1 pr-8 btn-base'>
      <option value='price.weekday:asc'>Sort by price: Low to High</option>
      <option value='price.weekday:desc'>Sort by price: High to Low</option>
    </select>
  )
}
