import { IMG_MAP } from 'shared/constants/AssetUrl'
import { AddressType } from 'shared/models/Address'

type PropsType = { addressData: AddressType }

export default function Address({ addressData }: PropsType) {
  return (
    <div className='space-y-2'>
      <h3>Địa chỉ</h3>
      <h6>{addressData.detail}</h6>
      <img src={IMG_MAP} alt='map' className='pt-2 cursor-not-allowed' />
    </div>
  )
}
