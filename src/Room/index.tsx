import { useState } from 'react'
import { useParams } from 'react-router-dom'
import { XIcon } from '@heroicons/react/outline'
import { RoomType } from 'shared/models/Room'
import useApi from 'shared/hooks/api'
import Navigation from 'shared/components/Navigation'
import Footer from 'shared/components/Footer'
import LoadingPage from 'shared/components/LoadingPage'
import Cover from './Cover'
import Amenities from './Amenities'
import Address from './Address'
import BookingForm from './BookingForm'

export default function Room() {
  const { id } = useParams()
  const { response, isLoading } = useApi.get(`/rooms/${id}`, false)
  const roomData: RoomType = response.data
  if (isLoading) return <LoadingPage />

  return (
    <>
      <Navigation />
      <Cover images={roomData.images} />
      <main className='flex max-w-[1100px] mx-auto mt-8 px-5'>
        <section className='grow space-y-6'>
          <h2>{roomData.name}</h2>
          <div dangerouslySetInnerHTML={{ __html: roomData.description }} />
          <Amenities amenitiesList={roomData.amenities} />
          <Address addressData={roomData.address} />
        </section>

        <section className='hidden md:block w-[360px] shrink-0 ml-8 my-12'>
          <div className='p-6 border rounded-md shadow'>
            <BookingForm roomData={roomData} />
          </div>
        </section>
      </main>

      <section className='md:hidden'>
        <MobileBookingForm roomData={roomData} />
      </section>

      <div className='mb-20 sm:mb-24 md:mb-0'>
        <Footer />
      </div>
    </>
  )
}

type MobileBookingType = { roomData: RoomType }

function MobileBookingForm({ roomData }: MobileBookingType) {
  const [formVisible, setFormVisible] = useState(false)
  const toggleVisible = () => setFormVisible((prev) => !prev)

  if (formVisible) {
    return (
      <div className='fixed z-50 inset-0 sm:top-auto sm:left-auto sm:w-[420px]'>
        <div className='relative sm:m-8 px-5 py-6 h-full border border-gray-400 rounded bg-white shadow-lg'>
          <div className='absolute top-2 right-2'>
            <button onClick={toggleVisible} className='btn-icon'>
              <XIcon className='h-6' />
            </button>
          </div>
          <BookingForm roomData={roomData} />
        </div>
      </div>
    )
  }
  return (
    <div className='fixed z-50 bottom-0 inset-x-0 sm:left-auto sm:w-[420px]'>
      <div className='sm:m-8 p-5 border border-gray-400 rounded bg-white shadow-lg'>
        <div className='flex justify-between items-center'>
          <h3>{roomData.price.weekday.toLocaleString()}đ / 1 đêm</h3>
          <button onClick={toggleVisible} className='button text-base'>
            Đặt ngay
          </button>
        </div>
      </div>
    </div>
  )
}
