import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import moment from 'moment'
import { useAppContext } from 'App/Context'
import { RoomType } from 'shared/models/Room'
import { FieldText } from 'shared/components/Form'
import { RESERVATION_URL } from 'shared/constants/ApiUrl'
import useApi from 'shared/hooks/api'
import { ReservationType } from 'shared/models/Reservation'

const formValidate = Yup.object({
  anotherName: Yup.string().required('Required').min(2, 'Invalid name'),
  anotherPhone: Yup.string()
    .required('Required')
    .matches(/^[0-9]{8,}$/, 'Invalid phone number'),
})

const dateInit = {
  start: moment().format('YYYY-MM-DD'),
  end: moment().add(1, 'day').format('YYYY-MM-DD'),
}

type PropsType = { roomData: RoomType }
type FormType = { anotherName: string; anotherPhone: string }

export default function BookingForm({ roomData }: PropsType) {
  const navigate = useNavigate()
  const { userData } = useAppContext()
  const [dateQuantity, setDateQuantity] = useState(1)
  const [bookingDate, setBookingDate] = useState(dateInit)
  const [response, postRequest] = useApi.post(RESERVATION_URL)
  const price = roomData.price.weekday

  const handleRequiredLogin = () => {
    if (!userData) {
      navigate('/login', { state: `/room/${roomData.id}` })
    }
  }

  const handleSubmit = async (formValues: FormType) => {
    const { anotherName, anotherPhone } = formValues
    const bookingData: ReservationType = {
      client: userData?.id as string,
      host: roomData.host_id,
      room: roomData.id,
      startDate: bookingDate.start,
      endDate: bookingDate.end,
      anotherName: anotherName !== userData?.firstName ? anotherName : '',
      anotherPhone: anotherPhone !== userData?.phone ? anotherPhone : '',
      status: 'pending',
    }
    await postRequest(bookingData)
  }

  const handleStartDate = (e: any) => {
    const start = e.target.value
    const { end } = bookingDate
    if (start < end) {
      const quantity = moment(end).diff(moment(start), 'day')
      setDateQuantity(quantity)
      setBookingDate({ start, end })
    } else {
      const end = moment(start).add(1, 'day').format('YYYY-MM-DD')
      setBookingDate({ start, end })
    }
  }

  const handleEndDate = (e: any) => {
    const end = e.target.value
    const { start } = bookingDate
    if (end > start) {
      const quantity = moment(end).diff(moment(start), 'day')
      setDateQuantity(quantity)
      setBookingDate({ start, end })
    }
  }

  const formInit: FormType = {
    anotherName: userData?.firstName || '',
    anotherPhone: userData?.phone || '',
  }

  return (
    <Formik
      enableReinitialize
      initialValues={formInit}
      validationSchema={formValidate}
      onSubmit={handleSubmit}
    >
      {({ touched, errors }) => (
        <Form>
          <h2>
            {price.toLocaleString()}
            <span className='text-sm'> / 1 đêm</span>
          </h2>

          <div className='mt-4 textbox'>
            <div className='flex items-center'>
              <input
                type='date'
                value={bookingDate.start}
                onChange={handleStartDate}
                className='textbox'
              />
              <span className='mx-2'>-</span>
              <input
                type='date'
                value={bookingDate.end}
                onChange={handleEndDate}
                className='textbox'
              />
            </div>
          </div>

          <div className='flex justify-between mt-4 mb-2'>
            <span>{`Giá thuê ${dateQuantity} đêm`}</span>
            <span>{(price * dateQuantity).toLocaleString()}</span>
          </div>

          <div className='flex justify-between mt-4 mb-2'>
            <span>Phí dịch vụ</span>
            <span>{(price * 0.1).toLocaleString()}</span>
          </div>
          <hr />

          <div className='flex justify-between mt-4 mb-2'>
            <b>Tổng tiền</b>
            <b>{(price * dateQuantity + price * 0.1).toLocaleString()}</b>
          </div>

          <div className='mt-6' onClick={handleRequiredLogin}>
            <FieldText
              sm
              placeholder='Tên của bạn'
              name='anotherName'
              error={touched.anotherName && errors.anotherName}
            />

            <div className='mt-5'>
              <FieldText
                sm
                placeholder='Số điện thoại'
                name='anotherPhone'
                error={touched.anotherPhone && errors.anotherPhone}
              />
            </div>

            {response.status.successful && (
              <small className='mt-2 text-green-500'>Đặt phòng thành công</small>
            )}
            {response.status.errorMessage && (
              <small className='mt-2 text-red-500'>Đặt phòng không thành công</small>
            )}

            <button type='submit' className='mt-6 mb-2 button w-full'>
              Đặt ngay
            </button>
          </div>
        </Form>
      )}
    </Formik>
  )
}
