import { AmenitiesList } from 'shared/constants/Amenities'

type PropsType = { amenitiesList: string[] }

export default function Amenities({ amenitiesList }: PropsType) {
  return (
    <>
      <h3 className='mb-3'>Tiện nghi</h3>
      <ul className='grid grid-cols-2 sm:grid-cols-3 gap-1 list-disc'>
        {amenitiesList.map((item) => (
          <AmenitiesItem key={item} id={item} />
        ))}
      </ul>
    </>
  )
}

function AmenitiesItem({ id }: { id: string }) {
  const itemData = AmenitiesList.filter((item) => item.id == id)[0]
  return <li className='ml-5'>{itemData?.name}</li>
}
