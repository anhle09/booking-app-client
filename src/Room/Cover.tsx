import { imageUrl } from 'shared/utils/luxstay'

type PropsType = {
  images: { id: number; url: string }[]
}

export default function Cover({ images }: PropsType) {
  return (
    <section className='flex overflow-auto scrollbar-none'>
      {images.map((image) => (
        <img
          key={image.url}
          alt='room-cover'
          src={imageUrl(image.url)}
          className='h-60 sm:h-80 pr-1 bg-black'
        />
      ))}
    </section>
  )
}
