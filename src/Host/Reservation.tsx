import moment from 'moment'
import { useAppContext } from 'App/Context'
import LoadingPage from 'shared/components/LoadingPage'
import StatusLabel from 'shared/components/StatusLabel'
import useApi from 'shared/hooks/api'
import { ReservationType } from 'shared/models/Reservation'
import { imageUrl } from 'shared/utils/luxstay'
import { RoomType } from 'shared/models/Room'
import { UserType } from 'shared/models/User'

export default function Reservation() {
  const { userData } = useAppContext()
  const { response, isLoading } = useApi.get(`/reservation?host=${userData?.id}`)
  const bookingList = response.data as ReservationType[]
  if (isLoading) return <LoadingPage />

  if (!bookingList?.length)
    return (
      <div className='max-w-xl min-w-md mx-auto px-4 sm:px-8'>
        <h2 className='mb-4'>Reservation</h2>
        <p>Reservation list is empty</p>
      </div>
    )

  return (
    <div className='max-w-xl min-w-md mx-auto px-4 sm:px-8'>
      <h2 className='mb-4'>Reservation</h2>

      <section className='border border-gray-200 rounded-lg overflow-hidden shadow'>
        <table className='w-full'>
          <thead className='bg-gray-50 text-left uppercase tracking-wider'>
            <tr className='text-gray-500 text-xs font-medium'>
              <th className='px-6 py-3'>Listing</th>
              <th className='px-6 py-3'>Location</th>
              <th className='px-6 py-3'>Guest</th>
              <th className='px-6 py-3'>Date</th>
              <th className='px-6 py-3'>Status</th>
              <th className='px-6 py-3'>Action</th>
            </tr>
          </thead>

          <tbody className='text-sm'>
            {bookingList.map((item) => (
              <ItemRow key={item.id} itemData={item} />
            ))}
          </tbody>
        </table>
      </section>
    </div>
  )
}

function ItemRow({ itemData }: { itemData: ReservationType }) {
  const startDate = moment(itemData.startDate).format('DD/MM/YYYY')
  const endDate = moment(itemData.endDate).format('DD/MM/YYYY')
  const roomData = itemData.room as RoomType
  const clientData = itemData.client as UserType

  return (
    <tr className='border-t'>
      <td className='flex items-center px-6 py-4'>
        <img
          className='h-10 w-10 aspect-square mr-4 rounded-full'
          src={imageUrl(roomData.cover_image)}
          alt='room-cover'
        />
        <h6>{roomData.name}</h6>
      </td>

      <td className='px-6 py-4'>
        {roomData.address.ward}, {roomData.address.district}
      </td>
      <td className='px-6 py-4 whitespace-nowrap'>
        {itemData.anotherName || clientData.firstName}
        <br />
        {itemData.anotherPhone || clientData.phone}
      </td>

      <td className='px-6 py-4 whitespace-nowrap'>
        {startDate}
        <br />
        {endDate}
      </td>

      <td className='px-6 py-4'>
        <StatusLabel label={itemData.status} />
      </td>

      <td className='px-6 py-4 text-center font-medium'>
        <span className='text-indigo-600 hover:text-indigo-900 cursor-not-allowed'>
          Edit
        </span>
      </td>
    </tr>
  )
}
