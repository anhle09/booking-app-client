import { useState } from 'react'
import { Outlet } from 'react-router-dom'
import { MenuIcon } from '@heroicons/react/outline'
import UserMenu from 'shared/components/Navigation/UserMenu'
import Sidebar from './Sidebar'

export default function HostLayout() {
  const [sidebarShow, setSidebarShow] = useState(false)
  const toggleSidebar = () => setSidebarShow((prev) => !prev)

  return (
    <>
      <Topbar toggleSidebar={toggleSidebar} />
      <Sidebar show={sidebarShow} onHide={toggleSidebar} />
      <main className='lg:ml-72 my-6'>
        <Outlet />
      </main>
    </>
  )
}

type TopbarType = { toggleSidebar: () => void }

const Topbar = ({ toggleSidebar }: TopbarType) => (
  <header className='h-16'>
    <div className='fixed z-40 top-0 right-0 left-0 lg:left-72 bg-white border-b'>
      <div className='flex items-center h-16 px-4 sm:px-8'>
        <div className='grow inline-flex'>
          <button className='btn-icon lg:hidden' onClick={toggleSidebar}>
            <MenuIcon width={24} />
          </button>
        </div>
        <UserMenu />
      </div>
    </div>
  </header>
)
