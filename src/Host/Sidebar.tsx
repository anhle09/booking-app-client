import { ReactNode } from 'react'
import { Link, useMatch } from 'react-router-dom'
import {
  CalendarIcon,
  ChatAltIcon,
  ClipboardCopyIcon,
  HomeIcon,
  SupportIcon,
  XIcon,
} from '@heroicons/react/outline'
import { LOGO_TEXT_WHITE } from 'shared/constants/AssetUrl'

type PropsType = {
  show: boolean
  onHide: () => void
}

export default function Sidebar({ show, onHide }: PropsType) {
  return (
    <aside className={`lg:block ${show ? 'block' : 'hidden'}`}>
      <div className='fixed z-50 inset-y-0 left-0 w-72 p-4 bg-gray-800'>
        <button onClick={onHide} className='float-right -mt-2 lg:hidden'>
          <XIcon width={24} color='lightgray' />
        </button>

        <Link to='/'>
          <img src={LOGO_TEXT_WHITE} className='mt-2 mb-6 h-10' alt='logo' />
        </Link>

        <nav onClick={onHide} className='space-y-1 text-gray-50'>
          <NavLink to='/host/listing'>
            <HomeIcon width={18} />
            <span>Listing</span>
          </NavLink>

          <NavLink to='/host/reservation'>
            <ClipboardCopyIcon width={18} />
            <span>Reservation</span>
          </NavLink>

          <NavLinkDisable>
            <ChatAltIcon width={18} />
            <span>Message</span>
          </NavLinkDisable>

          <NavLinkDisable>
            <CalendarIcon width={18} />
            <span>Calendar</span>
          </NavLinkDisable>

          <NavLinkDisable>
            <SupportIcon width={18} />
            <span>Help</span>
          </NavLinkDisable>
        </nav>
      </div>
    </aside>
  )
}

type NavLinkProps = {
  to: string
  children: ReactNode
}

function NavLink({ to, children }: NavLinkProps) {
  const matched = useMatch(to + '/*')
  const matchedClass = matched ? 'font-medium bg-gray-900' : 'hover:bg-gray-700'
  return (
    <div className={'rounded-md ' + matchedClass}>
      <Link to={to} className='flex items-center px-3 py-2 space-x-4'>
        {children}
      </Link>
    </div>
  )
}

function NavLinkDisable({ children }: { children: ReactNode }) {
  return (
    <div className='px-3 py-2 rounded-md cursor-not-allowed hover:bg-gray-700'>
      <div className='flex items-center space-x-4'>{children}</div>
    </div>
  )
}
