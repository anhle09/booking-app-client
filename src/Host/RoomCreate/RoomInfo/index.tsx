import { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { useAppContext } from 'App/Context'
import { FieldText, FieldTextarea } from 'shared/components/Form'
import { RoomType } from 'shared/models/Room'
import { HOST_URL } from 'shared/constants/ApiUrl'
import PhotoUpload from './PhotoUpload'
import FormFooter from './FormFooter'
import useApi from 'shared/hooks/api'

const formValidate = Yup.object({
  name: Yup.string().required('Required').min(8, 'Too short'),
  description: Yup.string().required('Required').min(8, 'Too short'),
})

export default function RoomInfo() {
  const navigate = useNavigate()
  const { roomId } = useParams()
  const { userData } = useAppContext()

  const [apiRes, multipartApi] = roomId
    ? useApi.multipartPatch(`/rooms/${roomId}`)
    : useApi.multipartPost('/rooms')

  const apiData = roomId ? useApi.get(`/rooms/${roomId}`) : null
  const roomData = apiData?.response.data as RoomType | null

  const [photoList, setPhotoList] = useState<string[]>([])
  const oldPhotoList = roomData?.images?.map((item) => HOST_URL + item.url)

  const handleReadFiles = (event: any) => {
    const files = event.target.files
    for (let i = 0; i < files.length; i++) {
      const fileReader = new FileReader()
      fileReader.onload = (e: any) => {
        const photoDataUrl = e.target.result
        setPhotoList((prev) => [...prev, photoDataUrl])
      }
      fileReader.readAsDataURL(files[i])
    }
  }

  const formOnSubmit = async (values: any) => {
    if (!values.files && !oldPhotoList) return
    const formData = {
      ...values,
      host_id: userData?.id,
      status: roomData?.status || 'pending',
    }
    const res = await multipartApi(formData)
    navigate(`/host/create/room-address/${res.data.id}`)
  }

  const formInit = {
    name: roomData?.name || '',
    description: roomData?.description || '',
  }

  return (
    <>
      <h2 className='mb-2'>Room details</h2>
      <h6>
        This information will be displayed publicly so be careful what you share
      </h6>

      <Formik
        enableReinitialize
        initialValues={formInit}
        validationSchema={formValidate}
        onSubmit={formOnSubmit}
      >
        {({ touched, errors, setFieldValue, submitCount, isSubmitting }) => (
          <Form>
            <div className='mt-6'>
              <FieldText
                label='Name'
                name='name'
                error={touched.name && errors.name}
              />
            </div>

            <section className='mt-6'>
              <FieldTextarea
                label='Description'
                name='description'
                error={touched.description && errors.description}
              />
              <div className='mt-2 text-gray-500'>
                Brief description for your profile. URLs are hyperlinked.
              </div>
            </section>

            <section className='mt-6'>
              <h4 className='mb-2'>Cover photo</h4>
              <PhotoUpload
                photoReview={
                  oldPhotoList ? [...oldPhotoList, ...photoList] : photoList
                }
                error={!!submitCount && !photoList.length && !oldPhotoList}
                handleFiles={(event: any) => {
                  setFieldValue('files', event.target.files)
                  handleReadFiles(event)
                }}
              />
            </section>

            <FormFooter apiStatus={apiRes.status} />
          </Form>
        )}
      </Formik>
    </>
  )
}
