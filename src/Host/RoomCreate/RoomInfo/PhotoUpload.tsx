type PropsType = {
  handleFiles: (e: any) => void
  photoReview: string[]
  error: boolean
}

export default function PhotoUpload({ handleFiles, photoReview, error }: PropsType) {
  return (
    <>
      <div className={error ? 'border-red-500' : 'border-gray-300'}>
        <div className='border-inherit border-2 border-dashed rounded-md'>
          <div className='p-2 flex justify-center flex-wrap'>
            {photoReview?.map((photo, index) => (
              <img key={index} src={photo} alt='room-cover' className='h-32 p-2' />
            ))}
            <PhotoInput handleFiles={handleFiles} />
          </div>
        </div>
      </div>
      <div className='text-error'>{error ? 'Required' : ''}</div>
    </>
  )
}

type PhotoInputType = { handleFiles: (e: any) => void }

export const PhotoInput = ({ handleFiles }: PhotoInputType) => (
  <label className='block p-4 space-y-1 text-center'>
    <PhotoUploadIcon />
    <div className='hover:text-indigo-500 focus-within:ring-indigo-500'>
      <span className='cursor-pointer font-medium text-sm text-indigo-600'>
        Upload photos
      </span>
      <input
        type='file'
        className='sr-only'
        accept='.jpg, .png'
        multiple
        onChange={handleFiles}
      />
    </div>
    <div className='text-xs text-gray-500'>PNG, JPG up to 10MB</div>
  </label>
)

const PhotoUploadIcon = () => (
  <svg
    className='mx-auto h-12 w-12 text-gray-400'
    stroke='currentColor'
    fill='none'
    viewBox='0 0 48 48'
    aria-hidden='true'
  >
    <path
      d='M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02'
      strokeWidth={2}
      strokeLinecap='round'
      strokeLinejoin='round'
    />
  </svg>
)
