import { Link } from 'react-router-dom'
import ApiStatus from 'shared/components/ApiStatus'
import Spinner from 'shared/components/Spinner'
import { ApiStatusType } from 'shared/models/Api'

type PropsType = { apiStatus: ApiStatusType }

export default function FormFooter({ apiStatus }: PropsType) {
  return (
    <div className='my-4'>
      <div className='my-6 float-right'>
        <button type='submit' className='button mr-4 space-x-2'>
          <span>Save details</span>
          {apiStatus.loading && <Spinner size='sm' />}
        </button>
        <Link
          to='/host'
          className='py-2 px-4 text-sm font-medium rounded-md border border-gray-600'
        >
          Cancel
        </Link>
      </div>
      <ApiStatus apiStatus={apiStatus} />
    </div>
  )
}
