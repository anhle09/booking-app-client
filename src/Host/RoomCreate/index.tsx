import { Link, Outlet, useLocation, useParams } from 'react-router-dom'
import { ChevronRightIcon } from '@heroicons/react/solid'

export default function RoomCreateLayout() {
  return (
    <>
      <Breadcrumbs />
      <div className='max-w-lg mx-auto px-4 sm:px-8'>
        <Outlet />
      </div>
    </>
  )
}

function Breadcrumbs() {
  const { roomId } = useParams()

  return (
    <nav className='fixed z-40 top-0 left-0 lg:left-72 ml-10 sm:ml-14 lg:ml-8'>
      <div className='flex items-center h-16 pl-4 lg:pl-0 text-sm font-medium'>
        <NavLink
          to={roomId ? `/host/create/room-info/${roomId}` : '/host/create'}
          label='Info'
        />
        <ChevronRightIcon className='h-5 mx-1 sm:mx-2 text-gray-400' />

        <NavLink
          to={`/host/create/room-address/${roomId}`}
          label='Address'
          disable={!roomId}
        />
        <ChevronRightIcon className='h-5 mx-1 sm:mx-2 text-gray-400' />

        <NavLink
          to={`/host/create/room-price/${roomId}`}
          label='Price'
          disable={!roomId}
        />
      </div>
    </nav>
  )
}

type NavLinkProps = {
  to: string
  label: string
  disable?: boolean
}

function NavLink({ to, label, disable }: NavLinkProps) {
  const { pathname } = useLocation()
  const active = pathname === to

  if (disable) {
    return (
      <div className={active ? 'text-gray-700' : 'text-gray-400'}>
        <span className='cursor-not-allowed'>{label}</span>
      </div>
    )
  } else {
    return (
      <div className={active ? 'text-gray-700' : 'text-gray-400'}>
        <Link to={to} className='hover:text-gray-700'>
          {label}
        </Link>
      </div>
    )
  }
}
