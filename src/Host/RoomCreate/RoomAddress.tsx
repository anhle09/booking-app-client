import { useNavigate, useParams } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import useApi from 'shared/hooks/api'
import { AddressType } from 'shared/models/Address'
import { FieldAutocomplete, FieldText, FieldTextarea } from 'shared/components/Form'
import { DistrictList, ProvinceList, WardList } from 'shared/constants/AddressBook'
import LoadingPage from 'shared/components/LoadingPage'
import FormFooter from './RoomInfo/FormFooter'

const formValidate = Yup.object({
  detail: Yup.string().required('Required').min(2, 'Too short'),
  ward: Yup.string().required('Required'),
  district: Yup.string().required('Required'),
  province: Yup.string().required('Required'),
})

export default function RoomAddress() {
  const { roomId } = useParams()
  const navigate = useNavigate()
  const [apiRes, patchRequest] = useApi.patch(`/rooms/${roomId}`)
  const { response, isLoading } = useApi.get(`/rooms/${roomId}`)
  const roomAddress: AddressType = response.data?.address
  if (isLoading) return <LoadingPage />

  const formOnSubmit = async (values: any) => {
    const res = await patchRequest({ address: values })
    if (res.data) {
      navigate(`/host/create/room-price/${roomId}`)
    }
  }

  const formInit: AddressType = {
    detail: roomAddress?.detail || '',
    province: roomAddress?.province || '',
    district: roomAddress?.district || '',
    ward: roomAddress?.ward || '',
    province_id: roomAddress?.province_id || 0,
    district_id: roomAddress?.district_id || 0,
    ward_id: roomAddress?.ward_id || 0,
  }

  const provinceInit = ProvinceList.filter((item) => item.id === formInit.province_id)[0]
  const districtInit = DistrictList.filter((item) => item.id === formInit.district_id)[0]
  const wardInit = WardList.filter((item) => item.id === formInit.ward_id)[0]

  return (
    <section>
      <h2 className='mb-2'>Address</h2>
      <h6>This information will be displayed publicly so be careful what you share</h6>

      <Formik
        enableReinitialize
        initialValues={formInit}
        validationSchema={formValidate}
        onSubmit={formOnSubmit}
      >
        {({ values, touched, errors, setFieldValue }) => (
          <Form>
            <div className='mt-6'>
              <FieldText
                label='Street'
                name='detail'
                error={touched.detail && errors.detail}
              />
            </div>

            <section className='grid sm:grid-cols-3 sm:gap-4'>
              <div className='mt-6'>
                <FieldAutocomplete
                  label='City/Province'
                  dataList={ProvinceList}
                  initialValue={provinceInit}
                  onChange={(value) => {
                    setFieldValue('province_id', value.id)
                    setFieldValue('province', value.name)
                  }}
                  error={touched.province && errors.province}
                />
              </div>

              <div className='mt-6'>
                <FieldAutocomplete
                  label='District'
                  initialValue={districtInit}
                  dataList={DistrictList.filter(
                    (item) => item.provinceId === values.province_id
                  )}
                  onChange={(value) => {
                    setFieldValue('district_id', value.id)
                    setFieldValue('district', value.name)
                  }}
                  error={touched.district && errors.district}
                />
              </div>

              <div className='mt-6'>
                <FieldAutocomplete
                  label='Ward'
                  initialValue={wardInit}
                  dataList={WardList.filter(
                    (item) => item.districtId === values.district_id
                  )}
                  onChange={(value) => {
                    setFieldValue('ward_id', value.id)
                    setFieldValue('ward', value.name)
                  }}
                  error={touched.ward && errors.ward}
                />
              </div>
            </section>

            <section className='mt-6'>
              <FieldTextarea
                label='Description'
                name='description'
                placeholder='Option'
              />
              <div className='mt-2 text-gray-500'>
                Brief description for your profile. URLs are hyperlinked.
              </div>
            </section>

            <FormFooter apiStatus={apiRes.status} />
          </Form>
        )}
      </Formik>
    </section>
  )
}
