import { useParams } from 'react-router-dom'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { FieldText, FieldSelect, FieldCheckbox } from 'shared/components/Form'
import { RoomType } from 'shared/models/Room'
import useApi from 'shared/hooks/api'
import LoadingPage from 'shared/components/LoadingPage'
import FormFooter from './RoomInfo/FormFooter'
import { AmenitiesList } from 'shared/constants/Amenities'

const formValidate = Yup.object({
  weekday: Yup.number().required('Required').typeError('Price must be a number'),
  weekend: Yup.number().required('Required').typeError('Price must be a number'),
})

export default function RoomPrice() {
  const { roomId } = useParams()
  const [apiRes, patchRequest] = useApi.patch(`/rooms/${roomId}`)
  const { response, isLoading } = useApi.get(`/rooms/${roomId}`)
  const roomData: RoomType = response.data
  if (isLoading) return <LoadingPage />

  const formOnSubmit = async (values: any) => {
    const { weekday, weekend, amenities } = values
    const roomData = {
      status: 'public',
      price: { weekday, weekend },
      amenities,
    }
    await patchRequest(roomData)
  }

  const formInit = {
    weekday: roomData?.price?.weekday || '',
    weekend: roomData?.price?.weekend || '',
    amenities: roomData?.amenities || [],
  }

  return (
    <section>
      <h2 className='mb-2'>Price</h2>
      <h6>This information will be displayed publicly so be careful what you share</h6>

      <Formik
        enableReinitialize
        initialValues={formInit}
        validationSchema={formValidate}
        onSubmit={formOnSubmit}
      >
        {({ touched, errors }) => (
          <Form>
            <div className='mt-6 grid sm:grid-cols-2 sm:gap-4'>
              <FieldSelect name='currency' label='Currency'>
                <option>VND</option>
                <option>USD</option>
              </FieldSelect>
            </div>

            <section className='grid sm:grid-cols-2 sm:gap-4'>
              <div className='mt-6'>
                <FieldText
                  label='Weekday'
                  name='weekday'
                  error={touched.weekday && errors.weekday}
                />
              </div>

              <div className='mt-6'>
                <FieldText
                  label='Weekend'
                  name='weekend'
                  error={touched.weekend && errors.weekend}
                />
              </div>
            </section>

            <div className='mt-8'>
              <h1 className='heading'>Amenities</h1>
              <h6 className='mt-2'>
                This information will be displayed publicly so be careful what you share.
              </h6>
            </div>

            <section className='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mt-4'>
              {AmenitiesList.map((item) => (
                <FieldCheckbox
                  key={item.id}
                  label={item.en}
                  name='amenities'
                  value={item.id}
                />
              ))}
            </section>

            <FormFooter apiStatus={apiRes.status} />
          </Form>
        )}
      </Formik>
    </section>
  )
}
