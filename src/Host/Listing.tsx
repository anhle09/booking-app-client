import { Link, useNavigate } from 'react-router-dom'
import { PencilAltIcon, TrashIcon } from '@heroicons/react/outline'
import { useAppContext } from 'App/Context'
import { RoomType } from 'shared/models/Room'
import { imageUrl } from 'shared/utils/luxstay'
import useApi from 'shared/hooks/api'
import LoadingPage from 'shared/components/LoadingPage'
import StatusLabel from 'shared/components/StatusLabel'
import Spinner from 'shared/components/Spinner'

export default function Listing() {
  const { userData } = useAppContext()
  const { response, isLoading } = useApi.get(`/rooms?host=${userData?.id}`)
  const roomList: RoomType[] = response.data
  if (isLoading) return <LoadingPage />

  return (
    <div className='max-w-xl min-w-md mx-auto px-4 sm:px-8'>
      <section className='flex justify-between items-center mb-6'>
        <h2>Room List</h2>
        <Link to='/host/create' className='button'>
          Create listing
        </Link>
      </section>

      {roomList?.length ? (
        <section className='border border-gray-200 rounded-lg overflow-hidden shadow'>
          <table className='w-full'>
            <thead className='bg-gray-50 text-left uppercase tracking-wider'>
              <tr className='text-gray-500 text-xs font-medium'>
                <th className='px-6 py-4'>Listing</th>
                <th className='px-6 py-4'>Location</th>
                <th className='px-6 py-4'>Weekday</th>
                <th className='px-6 py-4'>Weekend</th>
                <th className='px-6 py-4'>Status</th>
                <th className='px-6 py-4'>Action</th>
              </tr>
            </thead>
            <tbody className='text-sm'>
              {roomList?.map((item) => (
                <ListingRow key={item.id} roomData={item} />
              ))}
            </tbody>
          </table>
        </section>
      ) : (
        <p>Room list is empty</p>
      )}
    </div>
  )
}

function ListingRow({ roomData }: { roomData: RoomType }) {
  const navigate = useNavigate()
  const [apiRes, deleteRequest] = useApi.delete(`/rooms/${roomData.id}`)

  const handleEdit = () => navigate(`/host/create/room-info/${roomData.id}`)
  const handleDelete = async () => {
    await deleteRequest()
    navigate('/host')
  }

  const weekdayPrice = roomData.price?.weekday.toLocaleString()
  const weekendPrice = roomData.price?.weekend.toLocaleString()

  return (
    <tr className='border-t'>
      <td className='flex items-center px-6 py-4'>
        <img
          className='h-10 w-10 aspect-square mr-4 rounded-full'
          src={imageUrl(roomData.cover_image)}
          alt='room-cover'
        />
        <h6>{roomData.name}</h6>
      </td>

      <td className='px-6 py-4 whitespace-nowrap'>
        {roomData.address?.ward}
        <br />
        {roomData.address?.district}
      </td>

      <td className='px-6 py-4'>{weekdayPrice}</td>
      <td className='px-6 py-4'>{weekendPrice}</td>

      <td className='px-6 py-4'>
        <StatusLabel label={roomData.status} />
      </td>

      <td className='px-6 py-4'>
        <div className='flex space-x-2'>
          <PencilAltIcon
            width={20}
            className='text-indigo-600 cursor-pointer'
            onClick={handleEdit}
          />
          {apiRes.status.loading ? (
            <Spinner color='red' size='sm' />
          ) : (
            <TrashIcon
              width={20}
              className='text-red-500 cursor-pointer'
              onClick={handleDelete}
            />
          )}
        </div>
      </td>
    </tr>
  )
}
