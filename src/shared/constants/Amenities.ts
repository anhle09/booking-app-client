export const AmenitiesList = [
  { id: '0', name: 'Wifi', en: 'Wifi', icon: '' },
  { id: '1', name: 'Ti vi', en: 'Television', icon: '' },
  { id: '2', name: 'Máy lạnh', en: 'Air conditioner', icon: '' },
  { id: '3', name: 'Máy nước nóng', en: 'Water heater', icon: '' },
  { id: '4', name: 'Máy sấy tóc', en: 'Hair dryer', icon: '' },
  { id: '5', name: 'Hồ bơi', en: 'Swimming pool', icon: '' },
  { id: '6', name: 'Nhà bếp', en: 'Kitchen', icon: '' },
  { id: '7', name: 'Chổ đậu xe ô tô', en: 'Car parking', icon: '' },
]
