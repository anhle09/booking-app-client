export const ProvinceList = [
  {
    "id": 1,
    "name": "Thành phố Hà Nội",
    "text": "ha noi"
  },
  {
    "id": 32,
    "name": "Thành phố Đà Nẵng",
    "text": "da nang"
  },
  {
    "id": 44,
    "name": "Tỉnh Lâm Đồng",
    "text": "lam dong"
  },
  {
    "id": 49,
    "name": "Tỉnh Bà Rịa - Vũng Tàu",
    "text": "ba ria vung tau"
  },
  {
    "id": 50,
    "name": "Thành phố Hồ Chí Minh",
    "text": "ho chi minh"
  },
]

export const DistrictList = [
  {
    "id": 5,
    "provinceId": 1,
    "name": "Quận Cầu Giấy",
    "text": "quan cau giay",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 3,
    "provinceId": 1,
    "name": "Quận Tây Hồ",
    "text": "quan tay ho",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 2,
    "provinceId": 1,
    "name": "Quận Hoàn Kiếm",
    "text": "quan hoan kiem",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 1,
    "provinceId": 1,
    "name": "Quận Ba Đình",
    "text": "quan ba dinh",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 13,
    "provinceId": 1,
    "name": "Quận Nam Từ Liêm",
    "text": "quan nam tu liem",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 7,
    "provinceId": 1,
    "name": "Quận Hai Bà Trưng",
    "text": "quan hai ba trung",
    "province": "Thành phố Hà Nội",
  },
  {
    "id": 363,
    "provinceId": 32,
    "name": "Quận Sơn Trà",
    "text": "quan son tra",
    "province": "Thành phố Đà Nẵng",
  },
  {
    "id": 362,
    "provinceId": 32,
    "name": "Quận Hải Châu",
    "text": "quan hai chau",
    "province": "Thành phố Đà Nẵng",
  },
  {
    "id": 364,
    "provinceId": 32,
    "name": "Quận Ngũ Hành Sơn",
    "text": "quan ngu hanh son",
    "province": "Thành phố Đà Nẵng",
  },
  {
    "id": 496,
    "provinceId": 44,
    "name": "Thành phố Đà Lạt",
    "text": "da lat",
    "province": "Tỉnh Lâm Đồng",
  },
  {
    "id": 548,
    "provinceId": 49,
    "name": "Thành phố Vũng Tàu",
    "text": "vung tau",
    "province": "Tỉnh Bà Rịa - Vũng Tàu",
  },
  {
    "id": 574,
    "provinceId": 50,
    "name": "Quận 7",
    "text": "quan 7",
    "province": "Thành phố Hồ Chí Minh",
  },
  {
    "id": 556,
    "provinceId": 50,
    "name": "Quận 1",
    "text": "quan 1",
    "province": "Thành phố Hồ Chí Minh",
  },
  {
    "id": 569,
    "provinceId": 50,
    "name": "Quận 4",
    "text": "quan 4",
    "province": "Thành phố Hồ Chí Minh",
  },
  {
    "id": 561,
    "provinceId": 50,
    "name": "Quận Bình Thạnh",
    "text": "quan binh thanh",
    "province": "Thành phố Hồ Chí Minh",
  },
  {
    "id": 565,
    "provinceId": 50,
    "name": "Quận 2",
    "text": "quan 2",
    "province": "Thành phố Hồ Chí Minh",
  }
]

export const WardList = [
  {
    "id": 36,
    "districtId": 3,
    "provinceId": 1,
    "name": "Phường Quảng An",
    "text": "phuong quang an"
  },
  {
    "id": 20,
    "districtId": 2,
    "provinceId": 1,
    "name": "Phường Hàng Bồ",
    "text": "phuong hang bo"
  },
  {
    "id": 4,
    "districtId": 1,
    "provinceId": 1,
    "name": "Phường Cống Vị",
    "text": "phuong cong vi"
  },
  {
    "id": 34,
    "districtId": 3,
    "provinceId": 1,
    "name": "Phường Nhật Tân",
    "text": "phuong nhat tan"
  },
  {
    "id": 33,
    "districtId": 3,
    "provinceId": 1,
    "name": "Phường Phú Thượng",
    "text": "phuong phu thuong"
  },
  {
    "id": 207,
    "districtId": 13,
    "provinceId": 1,
    "name": "Phường Mễ Trì",
    "text": "phuong me tri"
  },
  {
    "id": 21,
    "districtId": 2,
    "provinceId": 1,
    "name": "Phường Cửa Đông",
    "text": "phuong cua dong"
  },
  {
    "id": 2,
    "districtId": 1,
    "provinceId": 1,
    "name": "Phường Trúc Bạch",
    "text": "phuong truc bach"
  },
  {
    "id": 27,
    "districtId": 2,
    "provinceId": 1,
    "name": "Phường Cửa Nam",
    "text": "phuong cua nam"
  },
  {
    "id": 59,
    "districtId": 5,
    "provinceId": 1,
    "name": "Phường Dịch Vọng Hậu",
    "text": "phuong dich vong hau"
  },
  {
    "id": 204,
    "districtId": 13,
    "provinceId": 1,
    "name": "Phường Mỹ Đình 1",
    "text": "phuong my dinh 1"
  },
  {
    "id": 84,
    "districtId": 7,
    "provinceId": 1,
    "name": "Phường Nguyễn Du",
    "text": "phuong nguyen du"
  },
  {
    "id": 6885,
    "districtId": 363,
    "provinceId": 32,
    "name": "Phường Phước Mỹ",
    "text": "phuong phuoc my"
  },
  {
    "id": 6880,
    "districtId": 362,
    "provinceId": 32,
    "name": "Phường Hòa Cường Nam",
    "text": "phuong hoa cuong nam"
  },
  {
    "id": 6869,
    "districtId": 362,
    "provinceId": 32,
    "name": "Phường Thuận Phước",
    "text": "phuong thuan phuoc"
  },
  {
    "id": 6888,
    "districtId": 364,
    "provinceId": 32,
    "name": "Phường Mỹ An",
    "text": "phuong my an"
  },
  {
    "id": 6884,
    "districtId": 363,
    "provinceId": 32,
    "name": "Phường An Hải Bắc",
    "text": "phuong an hai bac"
  },
  {
    "id": 6879,
    "districtId": 362,
    "provinceId": 32,
    "name": "Phường Hòa Cường Bắc",
    "text": "phuong hoa cuong bac"
  },
  {
    "id": 6886,
    "districtId": 363,
    "provinceId": 32,
    "name": "Phường An Hải Tây",
    "text": "phuong an hai tay"
  },
  {
    "id": 6870,
    "districtId": 362,
    "provinceId": 32,
    "name": "Phường Thạch Thang",
    "text": "phuong thach thang"
  },
  {
    "id": 6889,
    "districtId": 364,
    "provinceId": 32,
    "name": "Phường Khuê Mỹ",
    "text": "phuong khue my"
  },
  {
    "id": 8524,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 1",
    "text": "phuong 1"
  },
  {
    "id": 8522,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 9",
    "text": "phuong 9"
  },
  {
    "id": 8523,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 2",
    "text": "phuong 2"
  },
  {
    "id": 8530,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 3",
    "text": "phuong 3"
  },
  {
    "id": 8528,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 10",
    "text": "phuong 10"
  },
  {
    "id": 8527,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 4",
    "text": "phuong 4"
  },
  {
    "id": 8520,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 8",
    "text": "phuong 8"
  },
  {
    "id": 8525,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 6",
    "text": "phuong 6"
  },
  {
    "id": 8529,
    "districtId": 496,
    "provinceId": 44,
    "name": "Phường 11",
    "text": "phuong 11"
  },
  {
    "id": 9135,
    "districtId": 548,
    "provinceId": 49,
    "name": "Phường 2",
    "text": "phuong 2"
  },
  {
    "id": 9134,
    "districtId": 548,
    "provinceId": 49,
    "name": "Phường Thắng Tam",
    "text": "phuong thang tam"
  },
  {
    "id": 9146,
    "districtId": 548,
    "provinceId": 49,
    "name": "Phường 10",
    "text": "phuong 10"
  },
  {
    "id": 9138,
    "districtId": 548,
    "provinceId": 49,
    "name": "Phường 5",
    "text": "phuong 5"
  },
  {
    "id": 9142,
    "districtId": 548,
    "provinceId": 49,
    "name": "Phường 8",
    "text": "phuong 8"
  },
  {
    "id": 9472,
    "districtId": 574,
    "provinceId": 50,
    "name": "Phường Tân Phong",
    "text": "phuong tan phong"
  },
  {
    "id": 9216,
    "districtId": 556,
    "provinceId": 50,
    "name": "Phường Đa Kao",
    "text": "phuong da kao"
  },
  {
    "id": 9394,
    "districtId": 569,
    "provinceId": 50,
    "name": "Phường 12",
    "text": "phuong 12"
  },
  {
    "id": 9294,
    "districtId": 561,
    "provinceId": 50,
    "name": "Phường 22",
    "text": "phuong 22"
  },
  {
    "id": 9397,
    "districtId": 569,
    "provinceId": 50,
    "name": "Phường 06",
    "text": "phuong 06"
  },
  {
    "id": 9279,
    "districtId": 561,
    "provinceId": 50,
    "name": "Phường 27",
    "text": "phuong 27"
  },
  {
    "id": 9467,
    "districtId": 574,
    "provinceId": 50,
    "name": "Phường Tân Hưng",
    "text": "phuong tan hung"
  },
  {
    "id": 9223,
    "districtId": 556,
    "provinceId": 50,
    "name": "Phường Nguyễn Cư Trinh",
    "text": "phuong nguyen cu trinh"
  },
  {
    "id": 9286,
    "districtId": 561,
    "provinceId": 50,
    "name": "Phường 06",
    "text": "phuong 06"
  },
  {
    "id": 9339,
    "districtId": 565,
    "provinceId": 50,
    "name": "Phường An Phú",
    "text": "phuong an phu"
  }
]