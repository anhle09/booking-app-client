import { IGM_VUNGTAU, IMG_DALAT, IMG_DANANG, IMG_HANOI, IMG_HCM } from './AssetUrl'

export const TopLocation = [
  {
    id: 50,
    name: 'TP Hồ Chí Minh',
    type: 'province',
    image: IMG_HCM
  },
  {
    id: 1,
    name: 'Hà Nội',
    type: 'province',
    image: IMG_HANOI
  },
  {
    id: 32,
    name: 'Đà Nẵng',
    type: 'province',
    image: IMG_DANANG
  },
  {
    id: 548,
    name: 'Vũng Tàu',
    type: 'district',
    image: IGM_VUNGTAU
  },
  {
    id: 496,
    name: 'Đà Lạt',
    type: 'district',
    image: IMG_DALAT
  },
]

export const Suggestion = [
  {
    id: '61e27b723485392cd463fa78',
    image: 'https://cdn.luxstay.com/users/276795/TgNqpMj4DjdXLP6qAkgzzoii.jpg',
    name: 'The Royal Homies Suite Balcony - Phu My Hung'
  },
  {
    id: '61e27b723485392cd463fa7a',
    image: 'https://cdn.luxstay.com/rooms/12972/large/1528791976_a-34.jpg',
    name: 'Indochine Style Studio - Experience Locals Lifestyle'

  },
  {
    id: '61e27b723485392cd463fa7c',
    image: 'https://cdn.luxstay.com/rooms/26268/large/room_26268_13_1560937411.jpg',
    name: 'Stay&Fun - Căn hộ 40m2 cách Bitexco 10 phút đi bộ'
  },
  {
    id: '61e27b723485392cd463fa7e',
    image: 'https://cdn.luxstay.com/users/33926/G7oAB1SQInTZhIvz9_KY10Ed.jpg',
    name: 'Lux Millennium - View sông Sài Gòn - Free Gym & Pool'
  }
]
