import { HOST_URL } from 'shared/constants/ApiUrl'

const LUXSTAY_CDN = 'https://cdn.luxstay.com'

export function imageUrl(url: string) {
  if (!url) return
  if (url.includes('/images')) {
    return HOST_URL + url
  }
  else if (url.charAt(0) === '/') {
    return LUXSTAY_CDN + url
  }
  return LUXSTAY_CDN + '/' + url
}
