export default function objectToFormData(obj: any) {
  const formData  = new FormData()

  for (const key in obj) {
    if (key === 'files') {
      for (let i = 0; i < obj.files.length; i++) {
        formData.append('images', obj.files[i])
      }
    }
    else {
      formData.append(key, obj[key])
    }
  }
  return formData
}