export const getStoredAuthToken = () => {
  return localStorage.getItem('bookingApp_authToken')
}

export const storeAuthToken = (token: any) => {
  localStorage.setItem('bookingApp_authToken', token)
}

export const removeStoredAuthToken = () => {
  localStorage.removeItem('bookingApp_authToken')
}
