import { getStoredAuthToken } from "./authToken"
import { ResponseType } from "../models/Api"
import { API_URL } from 'shared/constants/ApiUrl'
import objectToFormData from './objectToFormData'

async function fetchApi(method: string, url: string, body?: any, multipart = false) {
  url = API_URL + url
  const token = getStoredAuthToken()
  let options = {}

  if (multipart) {
    options = {
      method: method,
      headers: { Authorization: token ? `Bearer ${token}` : undefined },
      body: objectToFormData(body),
    }
  }
  else {
    options = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
        Authorization: token ? `Bearer ${token}` : undefined,
      },
      body: body ? JSON.stringify(body) : undefined,
    }
  }

  try {
    const response = await fetch(url, options)
    const data = await response.json()
    return data as ResponseType
  }
  catch (error) {
    return {}
  }
}

export default fetchApi

export const api = {
  get: (url: string) => fetchApi('GET', url),
  post: (url: string, body: any) => fetchApi('POST', url, body),
  patch: (url: string, body: any) => fetchApi('PATCH', url, body),
  delete: (url: string) => fetchApi('DELETE', url),
}

export const multipartApi = {
  post: (url: string, body: any) => fetchApi('POST', url, body, true),
  patch: (url: string, body: any) => fetchApi('PATCH', url, body, true),
}
