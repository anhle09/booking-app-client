import { useState } from 'react'
import { Combobox } from '@headlessui/react'

export interface ItemType {
  id: number
  name: string
  text: string
}

export interface AutocompleteType {
  dataList: ItemType[]
  initialValue: ItemType
  error?: boolean | string
  onChange: (value: ItemType) => void
}

export default function Autocomplete({
  dataList,
  initialValue,
  error,
  onChange,
}: AutocompleteType) {
  const [selectedItem, setSelectedItem] = useState(initialValue)
  const [query, setQuery] = useState('')

  const queryItem = dataList.filter((item) =>
    item.text.toLowerCase().includes(query)
  )

  const handleChange = (value: ItemType) => {
    setSelectedItem(value)
    onChange(value)
  }

  return (
    <Combobox
      as='div'
      value={selectedItem}
      onChange={handleChange}
      className='relative'
    >
      <Combobox.Input
        onChange={(e) => setQuery(e.target.value)}
        displayValue={(selected: any) => selected?.name}
        className={error ? 'textbox-error h-[52px]' : 'textbox h-[52px]'}
      />
      <Combobox.Options className='menu-dropdown w-full'>
        {queryItem.map((item) => (
          <Combobox.Option key={item.id} value={item} className='menu-item'>
            {item.name}
          </Combobox.Option>
        ))}
        {!queryItem.length && <li className='menu-item'>No result</li>}
      </Combobox.Options>
    </Combobox>
  )
}
