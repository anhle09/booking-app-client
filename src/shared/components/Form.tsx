import { Field } from 'formik'
import Autocomplete, { AutocompleteType, ItemType } from './Autocomplete'

type FieldTextProps = {
  name: string
  placeholder?: string
  type?: string
  label?: string
  error?: any
  sm?: boolean
}

export const FieldText = ({ type, label, error, sm, ...rest }: FieldTextProps) => (
  <label>
    <h4 className='mb-2 text-gray-600'>{label}</h4>
    <Field
      {...rest}
      type={type || 'text'}
      className={error ? 'textbox-error' : 'textbox'}
      style={{ height: sm ? 42 : 52 }}
    />
    {error && <h6 className='text-error'>{error}</h6>}
  </label>
)

export const FieldTextarea = ({ label, error, ...rest }: FieldTextProps) => (
  <label>
    {label && <h4 className='mb-2 text-gray-600'>{label}</h4>}
    <Field
      {...rest}
      as='textarea'
      rows='3'
      className={error ? 'textbox-error' : 'textbox'}
    />
    {error && <h6 className='text-error'>{error}</h6>}
  </label>
)

type FieldSelectProps = {
  label: string
  name: string
  children: any
}

export const FieldSelect = ({ label, name, children }: FieldSelectProps) => (
  <label>
    <h4 className='mb-2 text-gray-600'>{label}</h4>
    <Field name={name} as='select' className='textbox'>
      {children}
    </Field>
  </label>
)

type FieldCheckboxProps = {
  label: string
  name: string
  value: string
}

export const FieldCheckbox = ({ label, ...rest }: FieldCheckboxProps) => (
  <label className='flex items-center'>
    <Field {...rest} type='checkbox' className='checkbox' />
    <span className='pl-3 text-sm text-gray-700'>{label}</span>
  </label>
)

interface FieldAutocompleteType extends AutocompleteType {
  label: string
  initialValue: ItemType
}

export const FieldAutocomplete = (props: FieldAutocompleteType) => {
  const { label, ...rest } = props
  return (
    <>
      {label && <h4 className='mb-2 text-gray-600'>{label}</h4>}
      <Autocomplete {...rest} />
      {props.error && <h6 className='text-error'>{props.error}</h6>}
    </>
  )
}
