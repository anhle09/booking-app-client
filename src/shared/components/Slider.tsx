import { useEffect, useRef, useState } from "react"


export default function Slider() {
  const [thumbOne, setThumbOne] = useState(0)
  const [thumbTwo, setThumbTwo] = useState(100)
  const range = useRef({ left: 0, right: 100 })

  useEffect(() => {
    if (thumbOne < thumbTwo)
      range.current = { left: thumbOne, right: thumbTwo }
    else
      range.current = { left: thumbTwo, right: thumbOne }
  }, [thumbOne, thumbTwo])

  return (
    <div className="relative">
      <input type="range" min="0" max="100" className="range-double"
        value={thumbOne}
        onChange={(e) => setThumbOne(parseInt(e.target.value))}
      />
      <input type="range" min="0" max="100" className="range-double"
        value={thumbTwo}
        onChange={(e) => setThumbTwo(parseInt(e.target.value))}
      />
      <div className="absolute inset-x-0 h-[6px] rounded-sm bg-gray-200" />
      <div className="absolute h-[6px] rounded-sm bg-indigo-500"
        style={{ left: `${range.current.left}%`, right: `${100 - range.current.right}%`}}
      />
    </div>
  )
}