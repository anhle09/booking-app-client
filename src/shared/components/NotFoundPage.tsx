export default function NotFoundPage() {
  return <h1 className='mt-20 text-center'>404 | Not found</h1>
}
