import Spinner from './Spinner'

export default function LoadingPage() {
  return (
    <div className='w-full h-screen flex justify-center items-center'>
      <span className='w-12 h-12'>
        <Spinner />
      </span>
    </div>
  )
}
