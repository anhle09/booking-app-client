import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { SearchIcon, LocationMarkerIcon } from '@heroicons/react/outline'
import useOnClickOutside from 'shared/hooks/onClickOutside'
import { DistrictList, ProvinceList } from 'shared/constants/AddressBook'

type SearchDataType = {
  id: number
  provinceId?: number
  name: string
  text: string
  province?: string
}

const dataList = [...ProvinceList, ...DistrictList]

export default function SearchBox() {
  const [searchMenuVisible, setSearchMenuVisible] = useState(false)
  const [searchData, setSearchData] = useState<SearchDataType[]>([])
  const [searchText, setSearchText] = useState('')
  const outsideWrapperRef = useOnClickOutside(() => setSearchMenuVisible(false))

  const handleHideMenu = (searchText: string) => {
    setSearchMenuVisible(false)
    setSearchData([])
    setSearchText(searchText)
  }

  const handleSearch = (text: string) => {
    setSearchText(text)
    if (text.length === 0) {
      handleHideMenu('')
    } else if (text.length > 1) {
      const searchResult = dataList.filter((item) => item.text.includes(text))
      !searchMenuVisible && setSearchMenuVisible(true)
      setSearchData(searchResult)
    }
  }

  return (
    <div ref={outsideWrapperRef} className='relative max-w-[340px]'>
      <label className='relative'>
        <input
          type='text'
          className='textbox leading-7 pl-12 shadow'
          placeholder='Tìm kiếm'
          value={searchText}
          onChange={(e) => handleSearch(e.target.value)}
        />
        <SearchIcon className='absolute top-0 h-5 mx-4 my-[13px] text-gray-500' />
      </label>
      {searchMenuVisible && (
        <SearchMenu searchData={searchData} handleHideMenu={handleHideMenu} />
      )}
    </div>
  )
}

type SearchMenuType = {
  searchData: SearchDataType[]
  handleHideMenu: (searchText: string) => void
}

function SearchMenu({ searchData, handleHideMenu }: SearchMenuType) {
  const navigate = useNavigate()

  const handleNavigate = (data: SearchDataType) => {
    handleHideMenu(data.name)
    let searchParam
    if (data.provinceId) {
      searchParam = `district=${data.id}`
    } else {
      searchParam = `province=${data.id}`
    }
    navigate(`/room?${searchParam}`)
  }

  return (
    <div className='absolute w-full mt-2 py-2 bg-white rounded-md shadow-lg border'>
      <ul className='text-sm text-gray-700 max-h-[400px] overflow-auto'>
        {searchData.map((item) => (
          <li
            key={item.id}
            className='flex items-center py-2 px-4 hover:bg-gray-100 cursor-pointer'
            onClick={() => handleNavigate(item)}
          >
            <LocationMarkerIcon className='h-4 mr-2' />
            <span>
              {item.name}
              {item.province && ` - ${item.province}`}
            </span>
          </li>
        ))}

        {!searchData.length && (
          <li className='py-2 px-4 hover:bg-gray-100 cursor-pointer'>
            No search result!
          </li>
        )}
      </ul>
    </div>
  )
}
