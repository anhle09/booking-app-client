import { Link } from 'react-router-dom'
import { Menu } from '@headlessui/react'
import { UserCircleIcon } from '@heroicons/react/solid'
import {
  CalendarIcon,
  CogIcon,
  LogoutIcon,
  ShieldCheckIcon,
} from '@heroicons/react/outline'
import { useAppContext } from 'App/Context'

export default function UserMenu() {
  const { userData, handleLogOut } = useAppContext()
  const userAvatar = userData?.avatar

  return (
    <Menu as='div' className='relative'>
      <Menu.Button className='flex items-center h-10 border border-gray-400 rounded-full'>
        <span className='ml-4 mr-2 text-gray-700'>{userData?.firstName}</span>
        {userAvatar ? (
          <img
            alt='avatar'
            src={userAvatar}
            className='w-7 h-7 ml-1 mr-3 object-cover rounded-full'
          />
        ) : (
          <UserCircleIcon className='h-8 mr-2 text-gray-500' />
        )}
      </Menu.Button>

      <Menu.Items as='div' className='menu-dropdown -right-2 w-56'>
        <Menu.Item as='div'>
          <Link to='/booking' className='menu-item flex items-center'>
            <CalendarIcon className='h-6 mr-3 text-gray-500' />
            <span>Đặt chổ của tôi</span>
          </Link>
        </Menu.Item>

        <Menu.Item as='div'>
          <Link to='/profile' className='menu-item flex items-center'>
            <CogIcon className='h-6 mr-3 text-gray-500' />
            <span>Cài đặt tài khoản</span>
          </Link>
        </Menu.Item>

        <Menu.Item as='div'>
          <Link to='/change-password' className='menu-item flex items-center'>
            <ShieldCheckIcon className='h-6 mr-3 text-gray-500' />
            <span>Đổi mật khẩu</span>
          </Link>
        </Menu.Item>

        <Menu.Item
          as='div'
          className='menu-item flex items-center cursor-pointer'
          onClick={handleLogOut}
        >
          <LogoutIcon className='h-6 mr-3 text-gray-500' />
          <span>Đăng xuất</span>
        </Menu.Item>
      </Menu.Items>
    </Menu>
  )
}
