import { Link } from 'react-router-dom'
import { useAppContext } from 'App/Context'
import Sidebar from './Sidebar'
import SearchBox from './SearchBox'
import UserMenu from './UserMenu'
import { LOGO_BLACK, LOGO_TEXT } from 'shared/constants/AssetUrl'

type PropsType = { searchBox?: boolean }

export default function Navigation({ searchBox }: PropsType) {
  const { userData } = useAppContext()

  return (
    <>
      <Sidebar />
      <header className='h-20'>
        <div className='fixed z-40 w-full bg-white border-b'>
          <div className='flex items-center h-20 max-w-[1400px] px-4 mr-16 sm:mx-auto'>
            <Link to='/'>
              {searchBox ? (
                <img
                  src={LOGO_BLACK}
                  className='h-12 mr-4 sm:h-14 sm:mr-6'
                  alt='logo'
                />
              ) : (
                <img src={LOGO_TEXT} className='h-12 mr-6' alt='logo' />
              )}
            </Link>
            <div className='grow'>{searchBox && <SearchBox />}</div>

            <nav className='hidden sm:flex items-center space-x-6 text-sm font-medium text-gray-600'>
              <Link to='/host' className='hidden md:block ml-4'>
                Trở thành chủ nhà
              </Link>
              {userData ? (
                <UserMenu />
              ) : (
                <>
                  <Link to='/register'>Đăng ký</Link>
                  <Link to='/login'>Đăng nhập</Link>
                </>
              )}
            </nav>
          </div>
        </div>
      </header>
    </>
  )
}
