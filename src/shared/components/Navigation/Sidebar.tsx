import { useState } from 'react'
import { Link } from 'react-router-dom'
import { UserCircleIcon } from '@heroicons/react/solid'
import {
  MenuIcon,
  XIcon,
  HomeIcon,
  ShieldCheckIcon,
  CalendarIcon,
  CogIcon,
  LogoutIcon,
} from '@heroicons/react/outline'
import { useAppContext } from 'App/Context'
import { LOGO_TEXT } from 'shared/constants/AssetUrl'

export default function Sidebar() {
  const [visible, setVisible] = useState(false)
  const toggleSidebar = () => setVisible((prev) => !prev)

  return (
    <aside className='sm:hidden'>
      <div className='fixed z-50 top-6 right-4'>
        <button className='btn-icon' onClick={toggleSidebar}>
          <MenuIcon className='h-7' />
        </button>
      </div>

      <section className={visible ? 'block' : 'hidden'}>
        <div className='fixed z-50 inset-0 -bottom-24 p-4 text-gray-700 bg-white'>
          <button onClick={toggleSidebar} className='mt-2 btn-icon float-right'>
            <XIcon className='h-8' />
          </button>
          <UserMenuList />

          <section className='my-4 p-5 space-y-3 border-t'>
            <h5 className='mt-2'>VỀ CHÚNG TÔI</h5>
            <p>Blog</p>
            <p>Điều kiện hoạt động</p>
            <p>Trang dành cho chủ nhà</p>
            <p>Cơ hội nghề nghiệp</p>
            <p>Tạp chí du lịch</p>
          </section>
        </div>
        {/* <div className="fixed z-40 inset-0 bg-gray-900 opacity-50" /> */}
      </section>
    </aside>
  )
}

function UserMenuList() {
  const { userData, handleLogOut } = useAppContext()
  const userAvatar = userData?.avatar

  if (userData)
    return (
      <>
        <div className='flex items-center mx-2 my-4'>
          {userAvatar ? (
            <img
              src={userAvatar}
              alt='avatar'
              className='w-12 h-12 mr-4 object-cover rounded-full'
            />
          ) : (
            <UserCircleIcon className='h-10 mr-3 text-gray-500' />
          )}
          <h3>{userData?.firstName}</h3>
        </div>

        <nav className='font-medium'>
          <Link to='/host' className='menu-item flex items-center'>
            <HomeIcon className='h-6 mr-3 text-gray-500' />
            <span>Trở thành chủ nhà</span>
          </Link>

          <Link to='/booking' className='menu-item flex items-center'>
            <CalendarIcon className='h-6 mr-3 text-gray-500' />
            <span>Đặt chổ của tôi</span>
          </Link>

          <Link to='/profile' className='menu-item flex items-center'>
            <CogIcon className='h-6 mr-3 text-gray-500' />
            <span>Cài đặt tài khoản</span>
          </Link>
          <Link to='/change-password' className='menu-item flex items-center'>
            <ShieldCheckIcon className='h-6 mr-3 text-gray-500' />
            <span>Đổi mật khẩu</span>
          </Link>
          <div
            onClick={handleLogOut}
            className='menu-item flex items-center cursor-pointer'
          >
            <LogoutIcon className='h-6 mr-3 text-gray-500' />
            <span>Đăng xuất</span>
          </div>
        </nav>
      </>
    )

  return (
    <>
      <Link to='/'>
        <img src={LOGO_TEXT} alt='logo' className='m-2 mb-4 h-10' />
      </Link>
      <nav className='px-5 font-medium'>
        <NavLink to='/host'>Trở thành chủ nhà</NavLink>
        <NavLink to='/register'>Đăng ký</NavLink>
        <NavLink to='/login'>Đăng nhập</NavLink>
      </nav>
    </>
  )
}

const NavLink = ({ to, children }: any) => (
  <Link to={to} className='block py-2 rounded-md active:bg-gray-200'>
    {children}
  </Link>
)
