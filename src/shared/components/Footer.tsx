import { ChatAlt2Icon, PhoneOutgoingIcon } from '@heroicons/react/outline'
import {
  LOGO_QR_CODE,
  LOGO_TEXT,
  SVG_APPLE_STORE,
  SVG_GOOGLE_STORE,
} from 'shared/constants/AssetUrl'

export default function Footer() {
  return (
    <footer className='max-w-xl mx-auto mt-8 bg-gray-100 sm:bg-white'>
      <div className='flex justify-around text-sm leading-8 py-8'>
        <section>
          <img src={LOGO_TEXT} alt='logo' className='hidden sm:block h-10' />
          <div className='flex mt-1 sm:mt-4'>
            <ChatAlt2Icon className='h-8 mx-2 text-gray-600' />
            <div className='leading-4 text-xs'>
              <h5>Messenger</h5>
              <a href='/'>http://m.me/luxstay</a>
            </div>
          </div>

          <div className='flex mt-4'>
            <PhoneOutgoingIcon className='h-8 mx-2 text-gray-600' />
            <div className='leading-4 text-xs'>
              <h5>Call center</h5>
              <span>18007799</span>
            </div>
          </div>
        </section>

        <section className='hidden lg:block'>
          <h5 className='mb-2'>TOP HOMESTAY ĐƯỢC YÊU THÍCH</h5>
          <a href='/' className='block'>
            Homestay Đà Lạt
          </a>
          <a href='/' className='block'>
            Homestay Hồ Chí Minh
          </a>
          <a href='/' className='block'>
            Homestay Hà Nội
          </a>
          <a href='/' className='block'>
            Homestay Đà Nẳng
          </a>
          <a href='/' className='block'>
            Homestay Vũng Tàu
          </a>
        </section>

        <section className='hidden xl:block'>
          <h5 className='mb-2'>KHÔNG GIAN ƯU THÍCH</h5>
          <a href='/' className='block'>
            Căn hộ dịch vụ
          </a>
          <a href='/' className='block'>
            Biệt thự
          </a>
          <a href='/' className='block'>
            Nhà riêng
          </a>
          <a href='/' className='block'>
            Studio
          </a>
          <a href='/' className='block'>
            Travel Guide
          </a>
        </section>

        <section className='hidden sm:block'>
          <h5 className='mb-2'>VỀ CHÚNG TÔI</h5>
          <a href='/' className='block'>
            Blog
          </a>
          <a href='/' className='block'>
            Điều kiện hoạt động
          </a>
          <a href='/' className='block'>
            Trang thông tin dành cho chủ nhà
          </a>
          <a href='/' className='block'>
            Cơ hội nghề nghiệp
          </a>
          <a href='/' className='block'>
            Tạp chí du lịch
          </a>
        </section>

        <section>
          <h5 className='mb-4'>TẢI ỨNG DỤNG</h5>
          <div className='flex items-center'>
            <img src={LOGO_QR_CODE} alt='qr-code' className='h-12' />
            <div className='ml-2 space-y-2'>
              <a href='/' className='block'>
                <img src={SVG_APPLE_STORE} alt='apple' className='h-6' />
              </a>
              <a href='/' className='block'>
                <img src={SVG_GOOGLE_STORE} alt='google' className='h-6' />
              </a>
            </div>
          </div>
        </section>
      </div>
    </footer>
  )
}
