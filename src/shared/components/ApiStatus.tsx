import { ApiStatusType } from 'shared/models/Api'

type PropsType = { apiStatus: ApiStatusType }

export default function ApiStatus({ apiStatus }: PropsType) {
  const { successful, errorMessage } = apiStatus
  if (successful) {
    return <small className='text-green-500'>Submit successfully</small>
  } else if (errorMessage) {
    return <small className='text-red-500'>{errorMessage}</small>
  }
  return null
}
