export default function StatusLabel({ label }: { label: string }) {
  return (
    <span className='px-2 inline-block text-xs leading-5 font-medium rounded-full bg-green-100 text-green-800'>
      {label}
    </span>
  )
}
