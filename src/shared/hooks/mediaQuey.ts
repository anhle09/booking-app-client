/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react"

export default function useMediaQuery(width: string) {
  const [matches, setMatches] = useState(false)

  useEffect(() => {
    const media = window.matchMedia(width)
    if (media.matches !== matches) {
      setMatches(media.matches)
    }
    const handleResize = () => setMatches(media.matches)
    window.addEventListener("resize", handleResize)
    return () => window.removeEventListener("resize", handleResize)
  }, [width])

  return matches
}
