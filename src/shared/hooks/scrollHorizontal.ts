import { useRef } from "react"

export default function useScrollHorizontal() {
  const divRef = useRef<HTMLDivElement>(null)

  const handleScrollLeft = (pixel: number) => {
    const element: any = divRef.current
    const { scrollLeft } = element

    if (scrollLeft > 0) {
      element?.scrollBy({
        top: 0,
        left: -pixel,
        behavior: 'smooth'
      })
    }
  }

  const handleScrollRight = (pixel: number) => {
    const element: any = divRef.current
    const { scrollLeft, scrollWidth, offsetWidth } = element

    if (scrollLeft < scrollWidth - offsetWidth) {
      element?.scrollBy({
        top: 0,
        left: pixel,
        behavior: 'smooth'
      })
    }
  }

  return ({ divRef, handleScrollLeft, handleScrollRight })
}
