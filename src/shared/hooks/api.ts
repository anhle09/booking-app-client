/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from 'react'
import useSWR from 'swr'
import fetchApi, { api } from '../utils/api'
import { ApiStatusType, ResponseType } from "../models/Api"

type MutationType = [
  { status: ApiStatusType, data: ResponseType },
  (body?: any) => Promise<ResponseType>
]

const apiStatus: ApiStatusType = {
  loading: false,
  successful: false,
}

function useQuery(url: string, revalidate = true) {
  const options = { revalidateIfStale: revalidate }
  const { data, error } = useSWR(url, api.get, options)
  return {
    response: data || {},
    isLoading: !error && !data
  }
}

function useMutation(method: string, url: string, multipart = false): MutationType {
  const [data, setData] = useState<ResponseType>({})
  const [status, setStatus] = useState(apiStatus)

  const apiRequest = async (body?: any) => {
    setStatus({ ...apiStatus, loading: true })
    const res = await fetchApi(method, url, body, multipart)
    if (res.data) {
      setData(res.data)
      setStatus({ ...apiStatus, successful: true })
    }
    else {
      setStatus({ ...apiStatus, errorMessage: res.error?.message })
    }
    return res
  }
  const response = { status, data }
  return [response, apiRequest]
}

const useApi = {
  get: useQuery,
  post: (url: string) => useMutation('POST', url),
  patch: (url: string) => useMutation('PATCH', url),
  delete: (url: string) => useMutation('DELETE', url),

  multipartPost: (url: string) => useMutation('POST', url, true),
  multipartPatch: (url: string) => useMutation('PATCH', url, true),
}

export default useApi
