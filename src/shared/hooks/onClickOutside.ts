import { useEffect, useRef } from 'react'

export default function useOnClickOutside(handleClickOutside: () => void) {
  const wrapperRef = useRef<HTMLDivElement>(null)

  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Escape') {
      handleClickOutside()
    }
  }

  const handleClick = (event: Event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target as Node)) {
      handleClickOutside()
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', handleKeyDown)
    document.addEventListener('click', handleClick)
    return () => {
      document.removeEventListener('keydown', handleKeyDown)
      document.removeEventListener('click', handleClick)
    }
  })

  return wrapperRef
}