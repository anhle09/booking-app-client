import qs from "query-string"
import { useSearchParams } from "react-router-dom"

export default function useQueryParam() {
  const [searchParams, setSearchParams] = useSearchParams()
  const queryString = searchParams.toString()
  const queryParam: any = qs.parse(queryString)

  const setQueryParam = (obj: any) => {
    setSearchParams(qs.stringify(obj))
    //setSearchParams(obj) // ko theo thu tu obj props
    //setSearchParams(qs.stringify(obj), { replace: true })
  }
  return { queryParam, setQueryParam }

  /* let queryParam: any = React.useMemo(() => qs.parse(queryString!), [queryString])
  let setValue = React.useCallback((newValue: any, options?: any) => {
      setSearchParams(newValue, options)
  }, []) */
}
