import { RoomType } from './Room'
import { UserType } from './User'

export type ReservationType = {
  id?: string,
  host: string,
  room: string | RoomType,
  client: string | UserType,
  anotherName?: string,
  anotherPhone?: string,
  startDate: string,
  endDate: string,
  status: string,
}