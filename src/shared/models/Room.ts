import { AddressType } from './Address'

export type RoomType = {
  id: string,
  host_id: string,
  name: string,
  description: string,
  cover_image: string,
  images: {
    id: number,
    url: string,
  }[],
  status: string,
  address: AddressType,
  amenities: string[],
  price: {
    max_guest: number,
    weekday: number,
    weekend: number,
  },
}
