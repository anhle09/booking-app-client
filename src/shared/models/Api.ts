export type ResponseType = {
  data?: any,
  page?: number,
  limit?: number,
  total?: number,
  error?: { message: string, statusCode?: number },
}

export type ApiStatusType = {
  loading: boolean,
  successful: boolean,
  errorMessage?: string,
}
