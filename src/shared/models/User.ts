export type UserType = {
  id: string,
  firstName: string,
  lastName: string,
  phone: string,
  avatar: string,
  email: string,
}